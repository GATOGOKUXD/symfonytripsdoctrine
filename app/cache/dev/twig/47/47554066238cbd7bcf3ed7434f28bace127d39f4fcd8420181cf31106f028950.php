<?php

/* @Framework/Form/percent_widget.html.php */
class __TwigTemplate_a265625080272da4040ac1fe21b960bcd63490e34db61a89b2a3c8d79c07dac6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_93474d9fb5e9b6c6ecc9914b19d70db2f439cd05aa4469c2246182ddef80d427 = $this->env->getExtension("native_profiler");
        $__internal_93474d9fb5e9b6c6ecc9914b19d70db2f439cd05aa4469c2246182ddef80d427->enter($__internal_93474d9fb5e9b6c6ecc9914b19d70db2f439cd05aa4469c2246182ddef80d427_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/percent_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple',  array('type' => isset(\$type) ? \$type : 'text')) ?> %
";
        
        $__internal_93474d9fb5e9b6c6ecc9914b19d70db2f439cd05aa4469c2246182ddef80d427->leave($__internal_93474d9fb5e9b6c6ecc9914b19d70db2f439cd05aa4469c2246182ddef80d427_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/percent_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple',  array('type' => isset($type) ? $type : 'text')) ?> %*/
/* */
