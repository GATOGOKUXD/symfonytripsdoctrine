<?php

/* TwigBundle:Exception:exception.js.twig */
class __TwigTemplate_08b3cb04d18ae7006c0b838b7bb58d9b4bada39347e7333b11affce290344a87 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fa85617b7eb960642307bf3193aa381baba031e6b43cffaa1aba71504e55c769 = $this->env->getExtension("native_profiler");
        $__internal_fa85617b7eb960642307bf3193aa381baba031e6b43cffaa1aba71504e55c769->enter($__internal_fa85617b7eb960642307bf3193aa381baba031e6b43cffaa1aba71504e55c769_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.js.twig"));

        // line 1
        echo "/*
";
        // line 2
        $this->loadTemplate("@Twig/Exception/exception.txt.twig", "TwigBundle:Exception:exception.js.twig", 2)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        // line 3
        echo "*/
";
        
        $__internal_fa85617b7eb960642307bf3193aa381baba031e6b43cffaa1aba71504e55c769->leave($__internal_fa85617b7eb960642307bf3193aa381baba031e6b43cffaa1aba71504e55c769_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.js.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  27 => 3,  25 => 2,  22 => 1,);
    }
}
/* /**/
/* {% include '@Twig/Exception/exception.txt.twig' with { 'exception': exception } %}*/
/* *//* */
/* */
