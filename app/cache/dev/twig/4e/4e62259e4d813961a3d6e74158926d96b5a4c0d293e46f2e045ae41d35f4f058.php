<?php

/* TwigBundle:Exception:error.txt.twig */
class __TwigTemplate_982087e5b2e6dcfbc1a501da8fc6d2a528bac97c129b4e22a02ec66ac0ccaadb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5f7f8662eecf11e76ea89ab4ecb9708b9bfca9044fe0c73a8399d4c9b3c43be1 = $this->env->getExtension("native_profiler");
        $__internal_5f7f8662eecf11e76ea89ab4ecb9708b9bfca9044fe0c73a8399d4c9b3c43be1->enter($__internal_5f7f8662eecf11e76ea89ab4ecb9708b9bfca9044fe0c73a8399d4c9b3c43be1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.txt.twig"));

        // line 1
        echo "Oops! An Error Occurred
=======================

The server returned a \"";
        // line 4
        echo (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code"));
        echo " ";
        echo (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text"));
        echo "\".

Something is broken. Please let us know what you were doing when this error occurred.
We will fix it as soon as possible. Sorry for any inconvenience caused.
";
        
        $__internal_5f7f8662eecf11e76ea89ab4ecb9708b9bfca9044fe0c73a8399d4c9b3c43be1->leave($__internal_5f7f8662eecf11e76ea89ab4ecb9708b9bfca9044fe0c73a8399d4c9b3c43be1_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.txt.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  27 => 4,  22 => 1,);
    }
}
/* Oops! An Error Occurred*/
/* =======================*/
/* */
/* The server returned a "{{ status_code }} {{ status_text }}".*/
/* */
/* Something is broken. Please let us know what you were doing when this error occurred.*/
/* We will fix it as soon as possible. Sorry for any inconvenience caused.*/
/* */
