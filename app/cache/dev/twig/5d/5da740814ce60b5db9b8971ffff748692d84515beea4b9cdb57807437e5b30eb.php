<?php

/* @Framework/FormTable/hidden_row.html.php */
class __TwigTemplate_554ce0f3f96e08f5affb25cd8661ad23ad60f08d9c577021e8c37e7a995cc7ca extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c3623366d6fbc192a3158d402919b4b0f47adb83c21041280ce3081b1418ebb6 = $this->env->getExtension("native_profiler");
        $__internal_c3623366d6fbc192a3158d402919b4b0f47adb83c21041280ce3081b1418ebb6->enter($__internal_c3623366d6fbc192a3158d402919b4b0f47adb83c21041280ce3081b1418ebb6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/hidden_row.html.php"));

        // line 1
        echo "<tr style=\"display: none\">
    <td colspan=\"2\">
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $__internal_c3623366d6fbc192a3158d402919b4b0f47adb83c21041280ce3081b1418ebb6->leave($__internal_c3623366d6fbc192a3158d402919b4b0f47adb83c21041280ce3081b1418ebb6_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/hidden_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <tr style="display: none">*/
/*     <td colspan="2">*/
/*         <?php echo $view['form']->widget($form) ?>*/
/*     </td>*/
/* </tr>*/
/* */
