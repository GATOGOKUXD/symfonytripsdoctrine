<?php

/* WebProfilerBundle:Profiler:toolbar_redirect.html.twig */
class __TwigTemplate_b7f60ef235aa37ed43289c2683130eff5383aa501df3ddb4f06ebcf85099c04d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_510acae85c04418c8d485a3860c5aa58b2e18114770cd904514d296432e15a9a = $this->env->getExtension("native_profiler");
        $__internal_510acae85c04418c8d485a3860c5aa58b2e18114770cd904514d296432e15a9a->enter($__internal_510acae85c04418c8d485a3860c5aa58b2e18114770cd904514d296432e15a9a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_510acae85c04418c8d485a3860c5aa58b2e18114770cd904514d296432e15a9a->leave($__internal_510acae85c04418c8d485a3860c5aa58b2e18114770cd904514d296432e15a9a_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_420247790356ccc130979ccae456f4e208abfae20a0429dd1644148ebcfd7fc6 = $this->env->getExtension("native_profiler");
        $__internal_420247790356ccc130979ccae456f4e208abfae20a0429dd1644148ebcfd7fc6->enter($__internal_420247790356ccc130979ccae456f4e208abfae20a0429dd1644148ebcfd7fc6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Redirection Intercepted";
        
        $__internal_420247790356ccc130979ccae456f4e208abfae20a0429dd1644148ebcfd7fc6->leave($__internal_420247790356ccc130979ccae456f4e208abfae20a0429dd1644148ebcfd7fc6_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_8ea7d927d04f7c53a23588fff0b1e808ca99b35654d4f63facece63a1bde18f4 = $this->env->getExtension("native_profiler");
        $__internal_8ea7d927d04f7c53a23588fff0b1e808ca99b35654d4f63facece63a1bde18f4->enter($__internal_8ea7d927d04f7c53a23588fff0b1e808ca99b35654d4f63facece63a1bde18f4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"";
        // line 8
        echo twig_escape_filter($this->env, (isset($context["location"]) ? $context["location"] : $this->getContext($context, "location")), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, (isset($context["location"]) ? $context["location"] : $this->getContext($context, "location")), "html", null, true);
        echo "</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
";
        
        $__internal_8ea7d927d04f7c53a23588fff0b1e808ca99b35654d4f63facece63a1bde18f4->leave($__internal_8ea7d927d04f7c53a23588fff0b1e808ca99b35654d4f63facece63a1bde18f4_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:toolbar_redirect.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 8,  53 => 6,  47 => 5,  35 => 3,  11 => 1,);
    }
}
/* {% extends '@Twig/layout.html.twig' %}*/
/* */
/* {% block title 'Redirection Intercepted' %}*/
/* */
/* {% block body %}*/
/*     <div class="sf-reset">*/
/*         <div class="block-exception">*/
/*             <h1>This request redirects to <a href="{{ location }}">{{ location }}</a>.</h1>*/
/* */
/*             <p>*/
/*                 <small>*/
/*                     The redirect was intercepted by the web debug toolbar to help debugging.*/
/*                     For more information, see the "intercept-redirects" option of the Profiler.*/
/*                 </small>*/
/*             </p>*/
/*         </div>*/
/*     </div>*/
/* {% endblock %}*/
/* */
