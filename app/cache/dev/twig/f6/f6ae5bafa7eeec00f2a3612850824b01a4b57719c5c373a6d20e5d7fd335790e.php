<?php

/* @Framework/Form/form_end.html.php */
class __TwigTemplate_e4b925aa7f3b97417d667066d51c6432f0d248092ec124fdc095d70ac7af9aa6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fc6dcbd0fd79f64ba94d200a02b26e6f2962b45c1c92b44e25545ae7489fb34b = $this->env->getExtension("native_profiler");
        $__internal_fc6dcbd0fd79f64ba94d200a02b26e6f2962b45c1c92b44e25545ae7489fb34b->enter($__internal_fc6dcbd0fd79f64ba94d200a02b26e6f2962b45c1c92b44e25545ae7489fb34b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_end.html.php"));

        // line 1
        echo "<?php if (!isset(\$render_rest) || \$render_rest): ?>
<?php echo \$view['form']->rest(\$form) ?>
<?php endif ?>
</form>
";
        
        $__internal_fc6dcbd0fd79f64ba94d200a02b26e6f2962b45c1c92b44e25545ae7489fb34b->leave($__internal_fc6dcbd0fd79f64ba94d200a02b26e6f2962b45c1c92b44e25545ae7489fb34b_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_end.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if (!isset($render_rest) || $render_rest): ?>*/
/* <?php echo $view['form']->rest($form) ?>*/
/* <?php endif ?>*/
/* </form>*/
/* */
