<?php

/* @Framework/Form/reset_widget.html.php */
class __TwigTemplate_289fb75bc7cf5bc04512146b52ad789c2df5882ebcdfee97c1e86e328da120f5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9514e86559a455d11b63f9cd7669eaa5519bfee5e6895be419907c5c997efc19 = $this->env->getExtension("native_profiler");
        $__internal_9514e86559a455d11b63f9cd7669eaa5519bfee5e6895be419907c5c997efc19->enter($__internal_9514e86559a455d11b63f9cd7669eaa5519bfee5e6895be419907c5c997efc19_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/reset_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'button_widget',  array('type' => isset(\$type) ? \$type : 'reset')) ?>
";
        
        $__internal_9514e86559a455d11b63f9cd7669eaa5519bfee5e6895be419907c5c997efc19->leave($__internal_9514e86559a455d11b63f9cd7669eaa5519bfee5e6895be419907c5c997efc19_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/reset_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'button_widget',  array('type' => isset($type) ? $type : 'reset')) ?>*/
/* */
