<?php

/* TwigBundle:Exception:error.css.twig */
class __TwigTemplate_30e47f83bd80f574be8911e7ac066e8f3dcc7062867ae2b1f38f2d7ba55460eb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c9b10a3ee8c4ce1e39600c9ac2f5118ce7dbb67964ae0aa370868b0693892e41 = $this->env->getExtension("native_profiler");
        $__internal_c9b10a3ee8c4ce1e39600c9ac2f5118ce7dbb67964ae0aa370868b0693892e41->enter($__internal_c9b10a3ee8c4ce1e39600c9ac2f5118ce7dbb67964ae0aa370868b0693892e41_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.css.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "css", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "css", null, true);
        echo "

*/
";
        
        $__internal_c9b10a3ee8c4ce1e39600c9ac2f5118ce7dbb67964ae0aa370868b0693892e41->leave($__internal_c9b10a3ee8c4ce1e39600c9ac2f5118ce7dbb67964ae0aa370868b0693892e41_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 2,  22 => 1,);
    }
}
/* /**/
/* {{ status_code }} {{ status_text }}*/
/* */
/* *//* */
/* */
