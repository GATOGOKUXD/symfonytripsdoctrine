<?php

/* @Framework/Form/search_widget.html.php */
class __TwigTemplate_ce4f5b800b47cbcc57e37979917e1ffb92d5427ea3e7623646185e6267863b16 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ee9ebc6b953b4cc430487a1015aa7049e5d16309aa402bf881e056eae32e1318 = $this->env->getExtension("native_profiler");
        $__internal_ee9ebc6b953b4cc430487a1015aa7049e5d16309aa402bf881e056eae32e1318->enter($__internal_ee9ebc6b953b4cc430487a1015aa7049e5d16309aa402bf881e056eae32e1318_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/search_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple',  array('type' => isset(\$type) ? \$type : 'search')) ?>
";
        
        $__internal_ee9ebc6b953b4cc430487a1015aa7049e5d16309aa402bf881e056eae32e1318->leave($__internal_ee9ebc6b953b4cc430487a1015aa7049e5d16309aa402bf881e056eae32e1318_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/search_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple',  array('type' => isset($type) ? $type : 'search')) ?>*/
/* */
