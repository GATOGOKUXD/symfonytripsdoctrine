<?php

/* @Framework/Form/url_widget.html.php */
class __TwigTemplate_9b2d8f482643237fa7115603997d2bda6c96deb75470db01897ef5912a80533c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a578283f9da94330da5de68b48167d636c9525f1c20dc1dfeef0b55c44ed61c4 = $this->env->getExtension("native_profiler");
        $__internal_a578283f9da94330da5de68b48167d636c9525f1c20dc1dfeef0b55c44ed61c4->enter($__internal_a578283f9da94330da5de68b48167d636c9525f1c20dc1dfeef0b55c44ed61c4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/url_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple',  array('type' => isset(\$type) ? \$type : 'url')) ?>
";
        
        $__internal_a578283f9da94330da5de68b48167d636c9525f1c20dc1dfeef0b55c44ed61c4->leave($__internal_a578283f9da94330da5de68b48167d636c9525f1c20dc1dfeef0b55c44ed61c4_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/url_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple',  array('type' => isset($type) ? $type : 'url')) ?>*/
/* */
