<?php

/* TwigBundle:Exception:error.xml.twig */
class __TwigTemplate_382547c1865e7ce229c87cc7e75f598eb2912c7191b3b30910f865e008119fd5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_49acfad961de1f4fc29facc708492530105a584d71c4ce7111566da3d51aee26 = $this->env->getExtension("native_profiler");
        $__internal_49acfad961de1f4fc29facc708492530105a584d71c4ce7111566da3d51aee26->enter($__internal_49acfad961de1f4fc29facc708492530105a584d71c4ce7111566da3d51aee26_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.xml.twig"));

        // line 1
        echo "<?xml version=\"1.0\" encoding=\"";
        echo twig_escape_filter($this->env, $this->env->getCharset(), "html", null, true);
        echo "\" ?>

<error code=\"";
        // line 3
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo "\" message=\"";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo "\" />
";
        
        $__internal_49acfad961de1f4fc29facc708492530105a584d71c4ce7111566da3d51aee26->leave($__internal_49acfad961de1f4fc29facc708492530105a584d71c4ce7111566da3d51aee26_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.xml.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 3,  22 => 1,);
    }
}
/* <?xml version="1.0" encoding="{{ _charset }}" ?>*/
/* */
/* <error code="{{ status_code }}" message="{{ status_text }}" />*/
/* */
