<?php

/* WebProfilerBundle:Profiler:ajax_layout.html.twig */
class __TwigTemplate_9792be69b67f4f8bc70e3752f4c7890829b70ca52ecc1753d210b00d1adea03d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_723fcb0fcb5217b2304cc55b13a078163dcfb8ccd4995df59c0af4044cb6ac4a = $this->env->getExtension("native_profiler");
        $__internal_723fcb0fcb5217b2304cc55b13a078163dcfb8ccd4995df59c0af4044cb6ac4a->enter($__internal_723fcb0fcb5217b2304cc55b13a078163dcfb8ccd4995df59c0af4044cb6ac4a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:ajax_layout.html.twig"));

        // line 1
        $this->displayBlock('panel', $context, $blocks);
        
        $__internal_723fcb0fcb5217b2304cc55b13a078163dcfb8ccd4995df59c0af4044cb6ac4a->leave($__internal_723fcb0fcb5217b2304cc55b13a078163dcfb8ccd4995df59c0af4044cb6ac4a_prof);

    }

    public function block_panel($context, array $blocks = array())
    {
        $__internal_6906e3a880d0b44f65ef7fb941738de47e6284954f844db7ad3add37619595af = $this->env->getExtension("native_profiler");
        $__internal_6906e3a880d0b44f65ef7fb941738de47e6284954f844db7ad3add37619595af->enter($__internal_6906e3a880d0b44f65ef7fb941738de47e6284954f844db7ad3add37619595af_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        echo "";
        
        $__internal_6906e3a880d0b44f65ef7fb941738de47e6284954f844db7ad3add37619595af->leave($__internal_6906e3a880d0b44f65ef7fb941738de47e6284954f844db7ad3add37619595af_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:ajax_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  23 => 1,);
    }
}
/* {% block panel '' %}*/
/* */
