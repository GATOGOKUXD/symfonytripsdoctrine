<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_fe95c9e75f357fe2cdd9399f4eec6373b07394906fb92e616554bda532e20492 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4cff345f59bec1d128453053cc79b7fecdd67281376cfe0cf527ec6bb0879b31 = $this->env->getExtension("native_profiler");
        $__internal_4cff345f59bec1d128453053cc79b7fecdd67281376cfe0cf527ec6bb0879b31->enter($__internal_4cff345f59bec1d128453053cc79b7fecdd67281376cfe0cf527ec6bb0879b31_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_4cff345f59bec1d128453053cc79b7fecdd67281376cfe0cf527ec6bb0879b31->leave($__internal_4cff345f59bec1d128453053cc79b7fecdd67281376cfe0cf527ec6bb0879b31_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_01c6322cdb1277ad8b45d38c96e11eca6e44f1335972a4109deb91a3f17afed2 = $this->env->getExtension("native_profiler");
        $__internal_01c6322cdb1277ad8b45d38c96e11eca6e44f1335972a4109deb91a3f17afed2->enter($__internal_01c6322cdb1277ad8b45d38c96e11eca6e44f1335972a4109deb91a3f17afed2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('request')->generateAbsoluteUrl($this->env->getExtension('asset')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_01c6322cdb1277ad8b45d38c96e11eca6e44f1335972a4109deb91a3f17afed2->leave($__internal_01c6322cdb1277ad8b45d38c96e11eca6e44f1335972a4109deb91a3f17afed2_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_593bcdc6cdceaa60239298107294597e45952a8141135745fa3c8cfd7812e3c2 = $this->env->getExtension("native_profiler");
        $__internal_593bcdc6cdceaa60239298107294597e45952a8141135745fa3c8cfd7812e3c2->enter($__internal_593bcdc6cdceaa60239298107294597e45952a8141135745fa3c8cfd7812e3c2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_593bcdc6cdceaa60239298107294597e45952a8141135745fa3c8cfd7812e3c2->leave($__internal_593bcdc6cdceaa60239298107294597e45952a8141135745fa3c8cfd7812e3c2_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_54003ed6f83ffdbdaac71e0e263da0e44ba7b417fac12d2f565aed6145443e5e = $this->env->getExtension("native_profiler");
        $__internal_54003ed6f83ffdbdaac71e0e263da0e44ba7b417fac12d2f565aed6145443e5e->enter($__internal_54003ed6f83ffdbdaac71e0e263da0e44ba7b417fac12d2f565aed6145443e5e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 12)->display($context);
        
        $__internal_54003ed6f83ffdbdaac71e0e263da0e44ba7b417fac12d2f565aed6145443e5e->leave($__internal_54003ed6f83ffdbdaac71e0e263da0e44ba7b417fac12d2f565aed6145443e5e_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@Twig/layout.html.twig' %}*/
/* */
/* {% block head %}*/
/*     <link href="{{ absolute_url(asset('bundles/framework/css/exception.css')) }}" rel="stylesheet" type="text/css" media="all" />*/
/* {% endblock %}*/
/* */
/* {% block title %}*/
/*     {{ exception.message }} ({{ status_code }} {{ status_text }})*/
/* {% endblock %}*/
/* */
/* {% block body %}*/
/*     {% include '@Twig/Exception/exception.html.twig' %}*/
/* {% endblock %}*/
/* */
