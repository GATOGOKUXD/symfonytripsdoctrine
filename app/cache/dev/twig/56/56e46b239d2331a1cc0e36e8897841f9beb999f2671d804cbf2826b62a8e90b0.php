<?php

/* @Twig/Exception/error.js.twig */
class __TwigTemplate_7a3666db8c06f8ad2f0814daf966f51e0129a7f492724a58f5c9d65b6ebcc18a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_556ce5f8258a6e091fe703eb5364fdf34f989ab02617e46cb26c3c73a3fe7838 = $this->env->getExtension("native_profiler");
        $__internal_556ce5f8258a6e091fe703eb5364fdf34f989ab02617e46cb26c3c73a3fe7838->enter($__internal_556ce5f8258a6e091fe703eb5364fdf34f989ab02617e46cb26c3c73a3fe7838_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.js.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "js", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "js", null, true);
        echo "

*/
";
        
        $__internal_556ce5f8258a6e091fe703eb5364fdf34f989ab02617e46cb26c3c73a3fe7838->leave($__internal_556ce5f8258a6e091fe703eb5364fdf34f989ab02617e46cb26c3c73a3fe7838_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/error.js.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 2,  22 => 1,);
    }
}
/* /**/
/* {{ status_code }} {{ status_text }}*/
/* */
/* *//* */
/* */
