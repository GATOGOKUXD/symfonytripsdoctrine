<?php

/* @Framework/Form/collection_widget.html.php */
class __TwigTemplate_57bbe3a936e954a12d7f993f6f2a6e2f2ed8c75a0fad445f4649367d5e3b437a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ff2ffb59b48b1fad1b8a0cd0b927e55ef0058ae191f61664365ae1c563220505 = $this->env->getExtension("native_profiler");
        $__internal_ff2ffb59b48b1fad1b8a0cd0b927e55ef0058ae191f61664365ae1c563220505->enter($__internal_ff2ffb59b48b1fad1b8a0cd0b927e55ef0058ae191f61664365ae1c563220505_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/collection_widget.html.php"));

        // line 1
        echo "<?php if (isset(\$prototype)): ?>
    <?php \$attr['data-prototype'] = \$view->escape(\$view['form']->row(\$prototype)) ?>
<?php endif ?>
<?php echo \$view['form']->widget(\$form, array('attr' => \$attr)) ?>
";
        
        $__internal_ff2ffb59b48b1fad1b8a0cd0b927e55ef0058ae191f61664365ae1c563220505->leave($__internal_ff2ffb59b48b1fad1b8a0cd0b927e55ef0058ae191f61664365ae1c563220505_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/collection_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if (isset($prototype)): ?>*/
/*     <?php $attr['data-prototype'] = $view->escape($view['form']->row($prototype)) ?>*/
/* <?php endif ?>*/
/* <?php echo $view['form']->widget($form, array('attr' => $attr)) ?>*/
/* */
