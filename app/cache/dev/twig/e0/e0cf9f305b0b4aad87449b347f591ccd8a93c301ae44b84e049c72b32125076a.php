<?php

/* TwigBundle:Exception:error.js.twig */
class __TwigTemplate_1c6acb6f2a95c4893e74b2279448c27c963157c8e3c0308db3b0d3b1c754f35e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c11c92d5b3ddfd05372890269b6ca4192215edc66f5e3d51587dc95942a51cd0 = $this->env->getExtension("native_profiler");
        $__internal_c11c92d5b3ddfd05372890269b6ca4192215edc66f5e3d51587dc95942a51cd0->enter($__internal_c11c92d5b3ddfd05372890269b6ca4192215edc66f5e3d51587dc95942a51cd0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.js.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "js", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "js", null, true);
        echo "

*/
";
        
        $__internal_c11c92d5b3ddfd05372890269b6ca4192215edc66f5e3d51587dc95942a51cd0->leave($__internal_c11c92d5b3ddfd05372890269b6ca4192215edc66f5e3d51587dc95942a51cd0_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.js.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 2,  22 => 1,);
    }
}
/* /**/
/* {{ status_code }} {{ status_text }}*/
/* */
/* *//* */
/* */
