<?php

/* @Framework/Form/container_attributes.html.php */
class __TwigTemplate_cba75b1e4bc478eefdd294941d3d5dfdc11c1bc0ea6b37eff75f991029b75fb0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_67e519c987d8f25f559807e0d0f1a668d46396b290d70f7ac10fb8ca301cba03 = $this->env->getExtension("native_profiler");
        $__internal_67e519c987d8f25f559807e0d0f1a668d46396b290d70f7ac10fb8ca301cba03->enter($__internal_67e519c987d8f25f559807e0d0f1a668d46396b290d70f7ac10fb8ca301cba03_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/container_attributes.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>
";
        
        $__internal_67e519c987d8f25f559807e0d0f1a668d46396b290d70f7ac10fb8ca301cba03->leave($__internal_67e519c987d8f25f559807e0d0f1a668d46396b290d70f7ac10fb8ca301cba03_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/container_attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'widget_container_attributes') ?>*/
/* */
