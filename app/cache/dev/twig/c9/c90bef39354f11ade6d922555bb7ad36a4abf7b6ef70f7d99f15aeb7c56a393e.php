<?php

/* @Twig/Exception/exception.css.twig */
class __TwigTemplate_739bc7e6bef1ea15929fe13b52491541cca2e5a17e971a683fb740e7c6c255d5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_acd4a5a7fc8943592fe2e560871ef0a42a8cd9c50aed12c6b84c765bb41e327d = $this->env->getExtension("native_profiler");
        $__internal_acd4a5a7fc8943592fe2e560871ef0a42a8cd9c50aed12c6b84c765bb41e327d->enter($__internal_acd4a5a7fc8943592fe2e560871ef0a42a8cd9c50aed12c6b84c765bb41e327d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception.css.twig"));

        // line 1
        echo "/*
";
        // line 2
        $this->loadTemplate("@Twig/Exception/exception.txt.twig", "@Twig/Exception/exception.css.twig", 2)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        // line 3
        echo "*/
";
        
        $__internal_acd4a5a7fc8943592fe2e560871ef0a42a8cd9c50aed12c6b84c765bb41e327d->leave($__internal_acd4a5a7fc8943592fe2e560871ef0a42a8cd9c50aed12c6b84c765bb41e327d_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  27 => 3,  25 => 2,  22 => 1,);
    }
}
/* /**/
/* {% include '@Twig/Exception/exception.txt.twig' with { 'exception': exception } %}*/
/* *//* */
/* */
