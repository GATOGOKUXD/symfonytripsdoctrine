<?php

/* @Twig/Exception/exception.js.twig */
class __TwigTemplate_63810e762a001fbaaa1df7e63035c4f90d33e5efa0af282556537fabf4083ef3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b9749417ccbb8f14e0a35dceea7c6f086d8577473add2ab17b0c4b455f66222c = $this->env->getExtension("native_profiler");
        $__internal_b9749417ccbb8f14e0a35dceea7c6f086d8577473add2ab17b0c4b455f66222c->enter($__internal_b9749417ccbb8f14e0a35dceea7c6f086d8577473add2ab17b0c4b455f66222c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception.js.twig"));

        // line 1
        echo "/*
";
        // line 2
        $this->loadTemplate("@Twig/Exception/exception.txt.twig", "@Twig/Exception/exception.js.twig", 2)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        // line 3
        echo "*/
";
        
        $__internal_b9749417ccbb8f14e0a35dceea7c6f086d8577473add2ab17b0c4b455f66222c->leave($__internal_b9749417ccbb8f14e0a35dceea7c6f086d8577473add2ab17b0c4b455f66222c_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception.js.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  27 => 3,  25 => 2,  22 => 1,);
    }
}
/* /**/
/* {% include '@Twig/Exception/exception.txt.twig' with { 'exception': exception } %}*/
/* *//* */
/* */
