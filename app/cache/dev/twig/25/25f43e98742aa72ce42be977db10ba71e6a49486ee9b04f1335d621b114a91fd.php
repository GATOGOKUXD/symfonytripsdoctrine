<?php

/* base.html.twig */
class __TwigTemplate_e734445b4ec0326f7fe1140c3523d2608907e574dfb73a136b693cafe71603fa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_37dac24d19da17d15d5eaff600554b0fd5355e8cddee27cdbe34082408f259bd = $this->env->getExtension("native_profiler");
        $__internal_37dac24d19da17d15d5eaff600554b0fd5355e8cddee27cdbe34082408f259bd->enter($__internal_37dac24d19da17d15d5eaff600554b0fd5355e8cddee27cdbe34082408f259bd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
 <head>

        <meta charset=\"utf-8\">
        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
        <meta name=\"description\" content=\"\">
        <meta name=\"author\" content=\"\">

        <title>Desarrollo web PHP</title>
        <!-- Bootstrap Core CSS -->
         <link href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/framework/css/bootstrap.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\">
        <!-- Custom CSS -->
         <link href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/framework/css/shop-homepage.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\"> 
        <!-- Superheroe CSS -->
           <link href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/framework/css/cerulean.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\"> 
        
        ";
        // line 19
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 22
        echo "    </head>
    
     
        <body>
 
        <!-- Navigation -->
        <nav class=\"navbar navbar-inverse navbar-fixed-top\" role=\"navigation\">
            <div class=\"container\">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class=\"navbar-header\">
                    <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-1\">
                        <span class=\"sr-only\">Toggle navigation</span>
                        <span class=\"icon-bar\"></span>
                        <span class=\"icon-bar\"></span>
                        <span class=\"icon-bar\"></span>
                    </button>
                    <a class=\"navbar-brand\" href=\"";
        // line 38
        echo $this->env->getExtension('routing')->getPath("trip_index");
        echo "\">Viajes 4v al mejor precio</a>
                </div>
                <!-- FORMULARIO DE BÚSQUEDA -->
                <form class=\"navbar-form navbar-right\" role=\"search\" method=\"get\"  action=\"";
        // line 41
        echo $this->env->getExtension('routing')->getPath("trip_search", array("busqueda" => "HOLA "));
        echo "\">
                    <div class=\"form-group\">
                        <input type=\"text\" name=\"trip\" class=\"form-control\" placeholder=\"Buscar...\" value=\"\">
                    </div>
                    <button type=\"submit\" class=\"btn btn-default\"><span class=\"glyphicon glyphicon-search\"></span></button>
                </form>
                <!-- FIN FORMULARIO DE BÚSQUEDA -->
            </div>
            <!-- /.container -->
        </nav>

        <!-- Page Content -->
        <div class=\"container\">

            <div class=\"row\">

                <div class=\"col-md-3\">
                    <p class=\"lead\">Tu agencia de viajes</p>
                    <div class=\"list-group\">
                       
                        <a href=\"";
        // line 61
        echo $this->env->getExtension('routing')->getPath("create_action");
        echo "\" class=\"list-group-item\">Sugiérenos un viaje</a>
                    </div>
                </div>

                <div class=\"col-md-9\">

                    <div class=\"row carousel-holder\">

                        <div class=\"col-md-12\">
                            <img class=\"slide-image img-rounded\" src=\"";
        // line 70
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/framework/img/logo.PNG"), "html", null, true);
        echo "\" alt=\"\">
                        </div>
                    </div>
                </div>
                     ";
        // line 74
        $this->displayBlock('body', $context, $blocks);
        // line 76
        echo "
        </div>

    </div>
    <!-- /.container -->

    <div class=\"container\">
  
        ";
        // line 84
        $this->displayBlock('javascripts', $context, $blocks);
        // line 87
        echo "          <!-- jQuery -->
          <script src=\"";
        // line 88
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/framework/js/jquery.js"), "html", null, true);
        echo "\"> </script>
    <!-- Bootstrap Core JavaScript -->
              <script src=\"";
        // line 90
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/framework/js/bootstrap.min.js"), "html", null, true);
        echo "\"> </script>
        <hr>

        <!-- Footer -->
        <footer>
            <div class=\"row\">
                <div class=\"col-lg-12\">
                    <p>&copy; Eugenia Pérez - 2015</p>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.container -->

 
        
        
        
    </body>
    
    
</html>
";
        
        $__internal_37dac24d19da17d15d5eaff600554b0fd5355e8cddee27cdbe34082408f259bd->leave($__internal_37dac24d19da17d15d5eaff600554b0fd5355e8cddee27cdbe34082408f259bd_prof);

    }

    // line 19
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_31e99b69b12facb2f3f15bcf723e2ff6c0be981c710ff9df86d8258e1e7a4475 = $this->env->getExtension("native_profiler");
        $__internal_31e99b69b12facb2f3f15bcf723e2ff6c0be981c710ff9df86d8258e1e7a4475->enter($__internal_31e99b69b12facb2f3f15bcf723e2ff6c0be981c710ff9df86d8258e1e7a4475_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 20
        echo "       
          ";
        
        $__internal_31e99b69b12facb2f3f15bcf723e2ff6c0be981c710ff9df86d8258e1e7a4475->leave($__internal_31e99b69b12facb2f3f15bcf723e2ff6c0be981c710ff9df86d8258e1e7a4475_prof);

    }

    // line 74
    public function block_body($context, array $blocks = array())
    {
        $__internal_827e09d296a70041437e626c31a7efc7a559d2f00a6e498f12885dbd725bdbd5 = $this->env->getExtension("native_profiler");
        $__internal_827e09d296a70041437e626c31a7efc7a559d2f00a6e498f12885dbd725bdbd5->enter($__internal_827e09d296a70041437e626c31a7efc7a559d2f00a6e498f12885dbd725bdbd5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 75
        echo "      ";
        
        $__internal_827e09d296a70041437e626c31a7efc7a559d2f00a6e498f12885dbd725bdbd5->leave($__internal_827e09d296a70041437e626c31a7efc7a559d2f00a6e498f12885dbd725bdbd5_prof);

    }

    // line 84
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_398b28cfeff0a8e2c1a1d41ed998069bbfc15b7d8a1a60045e6912cca7eafa19 = $this->env->getExtension("native_profiler");
        $__internal_398b28cfeff0a8e2c1a1d41ed998069bbfc15b7d8a1a60045e6912cca7eafa19->enter($__internal_398b28cfeff0a8e2c1a1d41ed998069bbfc15b7d8a1a60045e6912cca7eafa19_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 85
        echo "       
        ";
        
        $__internal_398b28cfeff0a8e2c1a1d41ed998069bbfc15b7d8a1a60045e6912cca7eafa19->leave($__internal_398b28cfeff0a8e2c1a1d41ed998069bbfc15b7d8a1a60045e6912cca7eafa19_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  208 => 85,  202 => 84,  195 => 75,  189 => 74,  181 => 20,  175 => 19,  144 => 90,  139 => 88,  136 => 87,  134 => 84,  124 => 76,  122 => 74,  115 => 70,  103 => 61,  80 => 41,  74 => 38,  56 => 22,  54 => 19,  49 => 17,  44 => 15,  39 => 13,  25 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html>*/
/*  <head>*/
/* */
/*         <meta charset="utf-8">*/
/*         <meta http-equiv="X-UA-Compatible" content="IE=edge">*/
/*         <meta name="viewport" content="width=device-width, initial-scale=1">*/
/*         <meta name="description" content="">*/
/*         <meta name="author" content="">*/
/* */
/*         <title>Desarrollo web PHP</title>*/
/*         <!-- Bootstrap Core CSS -->*/
/*          <link href="{{  asset('bundles/framework/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">*/
/*         <!-- Custom CSS -->*/
/*          <link href="{{ asset('bundles/framework/css/shop-homepage.css') }}" rel="stylesheet" type="text/css"> */
/*         <!-- Superheroe CSS -->*/
/*            <link href="{{ asset('bundles/framework/css/cerulean.min.css') }}" rel="stylesheet" type="text/css"> */
/*         */
/*         {% block stylesheets %}*/
/*        */
/*           {% endblock %}*/
/*     </head>*/
/*     */
/*      */
/*         <body>*/
/*  */
/*         <!-- Navigation -->*/
/*         <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">*/
/*             <div class="container">*/
/*                 <!-- Brand and toggle get grouped for better mobile display -->*/
/*                 <div class="navbar-header">*/
/*                     <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">*/
/*                         <span class="sr-only">Toggle navigation</span>*/
/*                         <span class="icon-bar"></span>*/
/*                         <span class="icon-bar"></span>*/
/*                         <span class="icon-bar"></span>*/
/*                     </button>*/
/*                     <a class="navbar-brand" href="{{path('trip_index')}}">Viajes 4v al mejor precio</a>*/
/*                 </div>*/
/*                 <!-- FORMULARIO DE BÚSQUEDA -->*/
/*                 <form class="navbar-form navbar-right" role="search" method="get"  action="{{ path('trip_search', { 'busqueda':'HOLA ' })}}">*/
/*                     <div class="form-group">*/
/*                         <input type="text" name="trip" class="form-control" placeholder="Buscar..." value="">*/
/*                     </div>*/
/*                     <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>*/
/*                 </form>*/
/*                 <!-- FIN FORMULARIO DE BÚSQUEDA -->*/
/*             </div>*/
/*             <!-- /.container -->*/
/*         </nav>*/
/* */
/*         <!-- Page Content -->*/
/*         <div class="container">*/
/* */
/*             <div class="row">*/
/* */
/*                 <div class="col-md-3">*/
/*                     <p class="lead">Tu agencia de viajes</p>*/
/*                     <div class="list-group">*/
/*                        */
/*                         <a href="{{ path('create_action') }}" class="list-group-item">Sugiérenos un viaje</a>*/
/*                     </div>*/
/*                 </div>*/
/* */
/*                 <div class="col-md-9">*/
/* */
/*                     <div class="row carousel-holder">*/
/* */
/*                         <div class="col-md-12">*/
/*                             <img class="slide-image img-rounded" src="{{ asset('bundles/framework/img/logo.PNG') }}" alt="">*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*                      {% block body %}*/
/*       {% endblock %}*/
/* */
/*         </div>*/
/* */
/*     </div>*/
/*     <!-- /.container -->*/
/* */
/*     <div class="container">*/
/*   */
/*         {% block javascripts %}*/
/*        */
/*         {% endblock %}*/
/*           <!-- jQuery -->*/
/*           <script src="{{ asset('bundles/framework/js/jquery.js') }}"> </script>*/
/*     <!-- Bootstrap Core JavaScript -->*/
/*               <script src="{{ asset('bundles/framework/js/bootstrap.min.js') }}"> </script>*/
/*         <hr>*/
/* */
/*         <!-- Footer -->*/
/*         <footer>*/
/*             <div class="row">*/
/*                 <div class="col-lg-12">*/
/*                     <p>&copy; Eugenia Pérez - 2015</p>*/
/*                 </div>*/
/*             </div>*/
/*         </footer>*/
/* */
/*     </div>*/
/*     <!-- /.container -->*/
/* */
/*  */
/*         */
/*         */
/*         */
/*     </body>*/
/*     */
/*     */
/* </html>*/
/* */
