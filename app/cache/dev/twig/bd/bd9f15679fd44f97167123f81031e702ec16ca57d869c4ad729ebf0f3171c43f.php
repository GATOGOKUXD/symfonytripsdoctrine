<?php

/* @Framework/Form/form_row.html.php */
class __TwigTemplate_ecf8025ae7176e1833eabb6627cbaab08e6e0596183b0d73f055b87be253a967 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bac02b7e008825ec3a49fe17da51daf2f6cbaf39f70c91f8496340704f0e3650 = $this->env->getExtension("native_profiler");
        $__internal_bac02b7e008825ec3a49fe17da51daf2f6cbaf39f70c91f8496340704f0e3650->enter($__internal_bac02b7e008825ec3a49fe17da51daf2f6cbaf39f70c91f8496340704f0e3650_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_row.html.php"));

        // line 1
        echo "<div>
    <?php echo \$view['form']->label(\$form) ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
";
        
        $__internal_bac02b7e008825ec3a49fe17da51daf2f6cbaf39f70c91f8496340704f0e3650->leave($__internal_bac02b7e008825ec3a49fe17da51daf2f6cbaf39f70c91f8496340704f0e3650_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <div>*/
/*     <?php echo $view['form']->label($form) ?>*/
/*     <?php echo $view['form']->errors($form) ?>*/
/*     <?php echo $view['form']->widget($form) ?>*/
/* </div>*/
/* */
