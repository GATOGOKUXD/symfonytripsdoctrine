<?php

/* @Framework/Form/form_rows.html.php */
class __TwigTemplate_29fee1e9b6b686cffad1f2f74ba111c00a892d909884b8f39f8d6d992c8611a9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f7f441b705c2438b56580507a228e5cbef11db37ab53563888c4e97526dc938f = $this->env->getExtension("native_profiler");
        $__internal_f7f441b705c2438b56580507a228e5cbef11db37ab53563888c4e97526dc938f->enter($__internal_f7f441b705c2438b56580507a228e5cbef11db37ab53563888c4e97526dc938f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rows.html.php"));

        // line 1
        echo "<?php foreach (\$form as \$child) : ?>
    <?php echo \$view['form']->row(\$child) ?>
<?php endforeach; ?>
";
        
        $__internal_f7f441b705c2438b56580507a228e5cbef11db37ab53563888c4e97526dc938f->leave($__internal_f7f441b705c2438b56580507a228e5cbef11db37ab53563888c4e97526dc938f_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_rows.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php foreach ($form as $child) : ?>*/
/*     <?php echo $view['form']->row($child) ?>*/
/* <?php endforeach; ?>*/
/* */
