<?php

/* WebProfilerBundle:Collector:exception.html.twig */
class __TwigTemplate_fcefa7d690e8b1023662c5c2c9e2b812828d49107c8199a1f233a704df4d1c0a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "WebProfilerBundle:Collector:exception.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ca7e8b1fbe57f30aceda981294d06ccce458614fb0e65055643618f60472698d = $this->env->getExtension("native_profiler");
        $__internal_ca7e8b1fbe57f30aceda981294d06ccce458614fb0e65055643618f60472698d->enter($__internal_ca7e8b1fbe57f30aceda981294d06ccce458614fb0e65055643618f60472698d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:exception.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ca7e8b1fbe57f30aceda981294d06ccce458614fb0e65055643618f60472698d->leave($__internal_ca7e8b1fbe57f30aceda981294d06ccce458614fb0e65055643618f60472698d_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_fcd4f61ea5af8794a63579bfabc92e23f25665a5285a9e5aa2adbf477348c5a6 = $this->env->getExtension("native_profiler");
        $__internal_fcd4f61ea5af8794a63579bfabc92e23f25665a5285a9e5aa2adbf477348c5a6->enter($__internal_fcd4f61ea5af8794a63579bfabc92e23f25665a5285a9e5aa2adbf477348c5a6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    ";
        if ($this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "hasexception", array())) {
            // line 5
            echo "        <style>
            ";
            // line 6
            echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('routing')->getPath("_profiler_exception_css", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
            echo "
        </style>
    ";
        }
        // line 9
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
";
        
        $__internal_fcd4f61ea5af8794a63579bfabc92e23f25665a5285a9e5aa2adbf477348c5a6->leave($__internal_fcd4f61ea5af8794a63579bfabc92e23f25665a5285a9e5aa2adbf477348c5a6_prof);

    }

    // line 12
    public function block_menu($context, array $blocks = array())
    {
        $__internal_09bbeba07b590e58d8cf36e1f61ce4c6125464ac15be040d03805cd5ad2aa574 = $this->env->getExtension("native_profiler");
        $__internal_09bbeba07b590e58d8cf36e1f61ce4c6125464ac15be040d03805cd5ad2aa574->enter($__internal_09bbeba07b590e58d8cf36e1f61ce4c6125464ac15be040d03805cd5ad2aa574_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 13
        echo "    <span class=\"label ";
        echo (($this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "hasexception", array())) ? ("label-status-error") : ("disabled"));
        echo "\">
        <span class=\"icon\">";
        // line 14
        echo twig_include($this->env, $context, "@WebProfiler/Icon/exception.svg");
        echo "</span>
        <strong>Exception</strong>
        ";
        // line 16
        if ($this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "hasexception", array())) {
            // line 17
            echo "            <span class=\"count\">
                <span>1</span>
            </span>
        ";
        }
        // line 21
        echo "    </span>
";
        
        $__internal_09bbeba07b590e58d8cf36e1f61ce4c6125464ac15be040d03805cd5ad2aa574->leave($__internal_09bbeba07b590e58d8cf36e1f61ce4c6125464ac15be040d03805cd5ad2aa574_prof);

    }

    // line 24
    public function block_panel($context, array $blocks = array())
    {
        $__internal_cd2ff7d77eec90af8e40278f7ae2d7328b2e9ab57a855829ce943bb768d7ca82 = $this->env->getExtension("native_profiler");
        $__internal_cd2ff7d77eec90af8e40278f7ae2d7328b2e9ab57a855829ce943bb768d7ca82->enter($__internal_cd2ff7d77eec90af8e40278f7ae2d7328b2e9ab57a855829ce943bb768d7ca82_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 25
        echo "    <h2>Exceptions</h2>

    ";
        // line 27
        if ( !$this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "hasexception", array())) {
            // line 28
            echo "        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    ";
        } else {
            // line 32
            echo "        <div class=\"sf-reset\">
            ";
            // line 33
            echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('routing')->getPath("_profiler_exception", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
            echo "
        </div>
    ";
        }
        
        $__internal_cd2ff7d77eec90af8e40278f7ae2d7328b2e9ab57a855829ce943bb768d7ca82->leave($__internal_cd2ff7d77eec90af8e40278f7ae2d7328b2e9ab57a855829ce943bb768d7ca82_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Collector:exception.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  117 => 33,  114 => 32,  108 => 28,  106 => 27,  102 => 25,  96 => 24,  88 => 21,  82 => 17,  80 => 16,  75 => 14,  70 => 13,  64 => 12,  54 => 9,  48 => 6,  45 => 5,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@WebProfiler/Profiler/layout.html.twig' %}*/
/* */
/* {% block head %}*/
/*     {% if collector.hasexception %}*/
/*         <style>*/
/*             {{ render(path('_profiler_exception_css', { token: token })) }}*/
/*         </style>*/
/*     {% endif %}*/
/*     {{ parent() }}*/
/* {% endblock %}*/
/* */
/* {% block menu %}*/
/*     <span class="label {{ collector.hasexception ? 'label-status-error' : 'disabled' }}">*/
/*         <span class="icon">{{ include('@WebProfiler/Icon/exception.svg') }}</span>*/
/*         <strong>Exception</strong>*/
/*         {% if collector.hasexception %}*/
/*             <span class="count">*/
/*                 <span>1</span>*/
/*             </span>*/
/*         {% endif %}*/
/*     </span>*/
/* {% endblock %}*/
/* */
/* {% block panel %}*/
/*     <h2>Exceptions</h2>*/
/* */
/*     {% if not collector.hasexception %}*/
/*         <div class="empty">*/
/*             <p>No exception was thrown and caught during the request.</p>*/
/*         </div>*/
/*     {% else %}*/
/*         <div class="sf-reset">*/
/*             {{ render(path('_profiler_exception', { token: token })) }}*/
/*         </div>*/
/*     {% endif %}*/
/* {% endblock %}*/
/* */
