<?php

/* @Framework/Form/hidden_row.html.php */
class __TwigTemplate_e6dac0606cf907794e417dd76eca7356f3074d46a7d27630cb512576387eaf91 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b14beb451e282a8227aaa1ba4a39337da9815fd92d9ad38b5c4cdf2fe76f5eca = $this->env->getExtension("native_profiler");
        $__internal_b14beb451e282a8227aaa1ba4a39337da9815fd92d9ad38b5c4cdf2fe76f5eca->enter($__internal_b14beb451e282a8227aaa1ba4a39337da9815fd92d9ad38b5c4cdf2fe76f5eca_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_row.html.php"));

        // line 1
        echo "<?php echo \$view['form']->widget(\$form) ?>
";
        
        $__internal_b14beb451e282a8227aaa1ba4a39337da9815fd92d9ad38b5c4cdf2fe76f5eca->leave($__internal_b14beb451e282a8227aaa1ba4a39337da9815fd92d9ad38b5c4cdf2fe76f5eca_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/hidden_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->widget($form) ?>*/
/* */
