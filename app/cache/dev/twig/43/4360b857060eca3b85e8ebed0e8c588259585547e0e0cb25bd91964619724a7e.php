<?php

/* TwigBundle:Exception:exception_full.html.twig */
class __TwigTemplate_68bac459390a88d16d8efe0e4f4f07f8c29356ff30bc21c132730b2cdc252a65 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "TwigBundle:Exception:exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f1e99f94501f781460405423b6f42302e94d6f2e8af06159474dcf8d39018ba2 = $this->env->getExtension("native_profiler");
        $__internal_f1e99f94501f781460405423b6f42302e94d6f2e8af06159474dcf8d39018ba2->enter($__internal_f1e99f94501f781460405423b6f42302e94d6f2e8af06159474dcf8d39018ba2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_f1e99f94501f781460405423b6f42302e94d6f2e8af06159474dcf8d39018ba2->leave($__internal_f1e99f94501f781460405423b6f42302e94d6f2e8af06159474dcf8d39018ba2_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_4fadc4bf8e2e1d637e0e69b2e0362e5f8750cdfd5fe7faee65887b5a846e89a0 = $this->env->getExtension("native_profiler");
        $__internal_4fadc4bf8e2e1d637e0e69b2e0362e5f8750cdfd5fe7faee65887b5a846e89a0->enter($__internal_4fadc4bf8e2e1d637e0e69b2e0362e5f8750cdfd5fe7faee65887b5a846e89a0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('request')->generateAbsoluteUrl($this->env->getExtension('asset')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_4fadc4bf8e2e1d637e0e69b2e0362e5f8750cdfd5fe7faee65887b5a846e89a0->leave($__internal_4fadc4bf8e2e1d637e0e69b2e0362e5f8750cdfd5fe7faee65887b5a846e89a0_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_ebfe0bc7dbaf2df6f024932ad840389e24d1ab965c538503e837c7777c23173e = $this->env->getExtension("native_profiler");
        $__internal_ebfe0bc7dbaf2df6f024932ad840389e24d1ab965c538503e837c7777c23173e->enter($__internal_ebfe0bc7dbaf2df6f024932ad840389e24d1ab965c538503e837c7777c23173e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_ebfe0bc7dbaf2df6f024932ad840389e24d1ab965c538503e837c7777c23173e->leave($__internal_ebfe0bc7dbaf2df6f024932ad840389e24d1ab965c538503e837c7777c23173e_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_e921c325687b091f3dd00f5dcb48438fb9dab6330eabafc70f7e056de61e70e4 = $this->env->getExtension("native_profiler");
        $__internal_e921c325687b091f3dd00f5dcb48438fb9dab6330eabafc70f7e056de61e70e4->enter($__internal_e921c325687b091f3dd00f5dcb48438fb9dab6330eabafc70f7e056de61e70e4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "TwigBundle:Exception:exception_full.html.twig", 12)->display($context);
        
        $__internal_e921c325687b091f3dd00f5dcb48438fb9dab6330eabafc70f7e056de61e70e4->leave($__internal_e921c325687b091f3dd00f5dcb48438fb9dab6330eabafc70f7e056de61e70e4_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@Twig/layout.html.twig' %}*/
/* */
/* {% block head %}*/
/*     <link href="{{ absolute_url(asset('bundles/framework/css/exception.css')) }}" rel="stylesheet" type="text/css" media="all" />*/
/* {% endblock %}*/
/* */
/* {% block title %}*/
/*     {{ exception.message }} ({{ status_code }} {{ status_text }})*/
/* {% endblock %}*/
/* */
/* {% block body %}*/
/*     {% include '@Twig/Exception/exception.html.twig' %}*/
/* {% endblock %}*/
/* */
