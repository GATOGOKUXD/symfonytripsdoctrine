<?php

/* trip/edit.html.twig */
class __TwigTemplate_163d39b4e8142124ab50cbe6f756b9305c4b3240be31ec6a475d83036e735868 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "trip/edit.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_39ea269056179b031fde3582edcfb8f8de89836c4e80d885583702e6045b99a5 = $this->env->getExtension("native_profiler");
        $__internal_39ea269056179b031fde3582edcfb8f8de89836c4e80d885583702e6045b99a5->enter($__internal_39ea269056179b031fde3582edcfb8f8de89836c4e80d885583702e6045b99a5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "trip/edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_39ea269056179b031fde3582edcfb8f8de89836c4e80d885583702e6045b99a5->leave($__internal_39ea269056179b031fde3582edcfb8f8de89836c4e80d885583702e6045b99a5_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_f34b16dc37f9817a41afa4830fc2f1a08bb8c2deaefa9092b0224c4666968fcd = $this->env->getExtension("native_profiler");
        $__internal_f34b16dc37f9817a41afa4830fc2f1a08bb8c2deaefa9092b0224c4666968fcd->enter($__internal_f34b16dc37f9817a41afa4830fc2f1a08bb8c2deaefa9092b0224c4666968fcd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Trip edit</h1>

    <form class=\"form-horizontal\" method=\"post\" action=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("trip_update", array("id" => $this->getAttribute((isset($context["trip"]) ? $context["trip"] : $this->getContext($context, "trip")), "id", array()))), "html", null, true);
        echo "\">
        <input type=\"hidden\" name=\"id\" value=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["trip"]) ? $context["trip"] : $this->getContext($context, "trip")), "id", array()), "html", null, true);
        echo "\" />
    <div class=\"form-group\">
        <label for=\"title\" class=\"col-sm-2 control-label\">NOMBRE</label>
        <div class=\"col-sm-10\">
            <input type=\"text\" class=\"form-control\" name=\"name\" id=\"title\" placeholder=\"Titulo\" value=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["trip"]) ? $context["trip"] : $this->getContext($context, "trip")), "name", array()), "html", null, true);
        echo "\">
        </div>
    </div>
    <div class=\"form-group\">
        <label for=\"cover\" class=\"col-sm-2 control-label\">DESCRIPCION</label>
        <div class=\"col-sm-10\">
            <input type=\"text\" class=\"form-control\" id=\"cover\" name=\"description\" placeholder=\"URL de la portada\" value=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["trip"]) ? $context["trip"] : $this->getContext($context, "trip")), "description", array()), "html", null, true);
        echo "\">
        </div>
    </div>
    <div class=\"form-group\">
        <label for=\"author\" class=\"col-sm-2 control-label\">URL IMAGEN</label>
        <div class=\"col-sm-10\">
            <input type=\"url\" class=\"form-control\" id=\"author\" name=\"url\" placeholder=\"Autor\" value=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["trip"]) ? $context["trip"] : $this->getContext($context, "trip")), "urlPicture", array()), "html", null, true);
        echo "\">
        </div>
    </div>
    <div class=\"form-group\">
        <label for=\"price\" class=\"col-sm-2 control-label\">PRECIO</label>
        <div class=\"col-sm-10\">
            <input type=\"number\" class=\"form-control\" id=\"price\" name=\"price\" placeholder=\"Precio\" value=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["trip"]) ? $context["trip"] : $this->getContext($context, "trip")), "price", array()), "html", null, true);
        echo "\">
        </div>
    </div>
    <div class=\"form-group\">
        <div class=\"col-sm-offset-2 col-sm-10\">
            <button type=\"submit\" class=\"btn btn-success pull-right\">UPDATE</button>
        </div>
    </div>
</form>
    
";
        
        $__internal_f34b16dc37f9817a41afa4830fc2f1a08bb8c2deaefa9092b0224c4666968fcd->leave($__internal_f34b16dc37f9817a41afa4830fc2f1a08bb8c2deaefa9092b0224c4666968fcd_prof);

    }

    public function getTemplateName()
    {
        return "trip/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  82 => 29,  73 => 23,  64 => 17,  55 => 11,  48 => 7,  44 => 6,  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends 'base.html.twig' %}*/
/* */
/* {% block body %}*/
/*     <h1>Trip edit</h1>*/
/* */
/*     <form class="form-horizontal" method="post" action="{{ path('trip_update', { 'id': trip.id }) }}">*/
/*         <input type="hidden" name="id" value="{{ trip.id}}" />*/
/*     <div class="form-group">*/
/*         <label for="title" class="col-sm-2 control-label">NOMBRE</label>*/
/*         <div class="col-sm-10">*/
/*             <input type="text" class="form-control" name="name" id="title" placeholder="Titulo" value="{{trip.name }}">*/
/*         </div>*/
/*     </div>*/
/*     <div class="form-group">*/
/*         <label for="cover" class="col-sm-2 control-label">DESCRIPCION</label>*/
/*         <div class="col-sm-10">*/
/*             <input type="text" class="form-control" id="cover" name="description" placeholder="URL de la portada" value="{{trip.description }}">*/
/*         </div>*/
/*     </div>*/
/*     <div class="form-group">*/
/*         <label for="author" class="col-sm-2 control-label">URL IMAGEN</label>*/
/*         <div class="col-sm-10">*/
/*             <input type="url" class="form-control" id="author" name="url" placeholder="Autor" value="{{trip.urlPicture }}">*/
/*         </div>*/
/*     </div>*/
/*     <div class="form-group">*/
/*         <label for="price" class="col-sm-2 control-label">PRECIO</label>*/
/*         <div class="col-sm-10">*/
/*             <input type="number" class="form-control" id="price" name="price" placeholder="Precio" value="{{trip.price }}">*/
/*         </div>*/
/*     </div>*/
/*     <div class="form-group">*/
/*         <div class="col-sm-offset-2 col-sm-10">*/
/*             <button type="submit" class="btn btn-success pull-right">UPDATE</button>*/
/*         </div>*/
/*     </div>*/
/* </form>*/
/*     */
/* {% endblock %}*/
/* */
