<?php

/* @Framework/Form/button_row.html.php */
class __TwigTemplate_b88d0145af7e45911c1990bc9a8786cde63fe1ede61158ac9ae5dffff54261e1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1d5a9a1530b52eeaa1c63cbadc7aa19c937af383e9edcb2661637524c5159eed = $this->env->getExtension("native_profiler");
        $__internal_1d5a9a1530b52eeaa1c63cbadc7aa19c937af383e9edcb2661637524c5159eed->enter($__internal_1d5a9a1530b52eeaa1c63cbadc7aa19c937af383e9edcb2661637524c5159eed_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_row.html.php"));

        // line 1
        echo "<div>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
";
        
        $__internal_1d5a9a1530b52eeaa1c63cbadc7aa19c937af383e9edcb2661637524c5159eed->leave($__internal_1d5a9a1530b52eeaa1c63cbadc7aa19c937af383e9edcb2661637524c5159eed_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/button_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <div>*/
/*     <?php echo $view['form']->widget($form) ?>*/
/* </div>*/
/* */
