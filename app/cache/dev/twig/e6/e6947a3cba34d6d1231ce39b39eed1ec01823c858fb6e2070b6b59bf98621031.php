<?php

/* @Framework/Form/form_widget_compound.html.php */
class __TwigTemplate_ee022eb7fd6367c4eca204ae7439b0d79fa0cb7e266a9902f02e4e45a8aeca5c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_02faef935dc2e340d5cbf923a1ecc00abb94b0ecf4aae28a4221cca3b20cd6e8 = $this->env->getExtension("native_profiler");
        $__internal_02faef935dc2e340d5cbf923a1ecc00abb94b0ecf4aae28a4221cca3b20cd6e8->enter($__internal_02faef935dc2e340d5cbf923a1ecc00abb94b0ecf4aae28a4221cca3b20cd6e8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget_compound.html.php"));

        // line 1
        echo "<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php endif ?>
    <?php echo \$view['form']->block(\$form, 'form_rows') ?>
    <?php echo \$view['form']->rest(\$form) ?>
</div>
";
        
        $__internal_02faef935dc2e340d5cbf923a1ecc00abb94b0ecf4aae28a4221cca3b20cd6e8->leave($__internal_02faef935dc2e340d5cbf923a1ecc00abb94b0ecf4aae28a4221cca3b20cd6e8_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget_compound.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <div <?php echo $view['form']->block($form, 'widget_container_attributes') ?>>*/
/*     <?php if (!$form->parent && $errors): ?>*/
/*     <?php echo $view['form']->errors($form) ?>*/
/*     <?php endif ?>*/
/*     <?php echo $view['form']->block($form, 'form_rows') ?>*/
/*     <?php echo $view['form']->rest($form) ?>*/
/* </div>*/
/* */
