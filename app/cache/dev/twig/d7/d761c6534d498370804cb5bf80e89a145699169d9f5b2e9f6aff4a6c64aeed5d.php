<?php

/* WebProfilerBundle:Collector:router.html.twig */
class __TwigTemplate_f5ad7255b9852d1e109c995c12ff11c7235ca772cbd9f0d23b76ce53ca0a3e1f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "WebProfilerBundle:Collector:router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3a994698f347b38c96b39cdc2f0038cccbe4bb326fe7d696bb86df1f1df284b7 = $this->env->getExtension("native_profiler");
        $__internal_3a994698f347b38c96b39cdc2f0038cccbe4bb326fe7d696bb86df1f1df284b7->enter($__internal_3a994698f347b38c96b39cdc2f0038cccbe4bb326fe7d696bb86df1f1df284b7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_3a994698f347b38c96b39cdc2f0038cccbe4bb326fe7d696bb86df1f1df284b7->leave($__internal_3a994698f347b38c96b39cdc2f0038cccbe4bb326fe7d696bb86df1f1df284b7_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_ff0ea5944d4986dd40b3c02492ecf98108ff5b8e155e2cbe87e6d34a9652d002 = $this->env->getExtension("native_profiler");
        $__internal_ff0ea5944d4986dd40b3c02492ecf98108ff5b8e155e2cbe87e6d34a9652d002->enter($__internal_ff0ea5944d4986dd40b3c02492ecf98108ff5b8e155e2cbe87e6d34a9652d002_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_ff0ea5944d4986dd40b3c02492ecf98108ff5b8e155e2cbe87e6d34a9652d002->leave($__internal_ff0ea5944d4986dd40b3c02492ecf98108ff5b8e155e2cbe87e6d34a9652d002_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_a8d8d773a260a2513151d48e81c4d5229f244b0c4bdb5a9f0d617cdf26c82031 = $this->env->getExtension("native_profiler");
        $__internal_a8d8d773a260a2513151d48e81c4d5229f244b0c4bdb5a9f0d617cdf26c82031->enter($__internal_a8d8d773a260a2513151d48e81c4d5229f244b0c4bdb5a9f0d617cdf26c82031_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_a8d8d773a260a2513151d48e81c4d5229f244b0c4bdb5a9f0d617cdf26c82031->leave($__internal_a8d8d773a260a2513151d48e81c4d5229f244b0c4bdb5a9f0d617cdf26c82031_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_7b68633a61237d04a7a55d867d42b112ec4b5616d6d9b7ebe099d16e7c28d7fc = $this->env->getExtension("native_profiler");
        $__internal_7b68633a61237d04a7a55d867d42b112ec4b5616d6d9b7ebe099d16e7c28d7fc->enter($__internal_7b68633a61237d04a7a55d867d42b112ec4b5616d6d9b7ebe099d16e7c28d7fc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('routing')->getPath("_profiler_router", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_7b68633a61237d04a7a55d867d42b112ec4b5616d6d9b7ebe099d16e7c28d7fc->leave($__internal_7b68633a61237d04a7a55d867d42b112ec4b5616d6d9b7ebe099d16e7c28d7fc_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Collector:router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 13,  67 => 12,  56 => 7,  53 => 6,  47 => 5,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@WebProfiler/Profiler/layout.html.twig' %}*/
/* */
/* {% block toolbar %}{% endblock %}*/
/* */
/* {% block menu %}*/
/* <span class="label">*/
/*     <span class="icon">{{ include('@WebProfiler/Icon/router.svg') }}</span>*/
/*     <strong>Routing</strong>*/
/* </span>*/
/* {% endblock %}*/
/* */
/* {% block panel %}*/
/*     {{ render(path('_profiler_router', { token: token })) }}*/
/* {% endblock %}*/
/* */
