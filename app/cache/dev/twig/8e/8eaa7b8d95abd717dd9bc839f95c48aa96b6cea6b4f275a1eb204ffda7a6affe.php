<?php

/* @Framework/Form/email_widget.html.php */
class __TwigTemplate_a5adde898643d61a60a76d59333a22651a3eaa1d85d4437653a9c2cf553239c7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a76299bd463d5a30a10291bfb35387049a53d410c8f7861aa92f330e4c9c3d0d = $this->env->getExtension("native_profiler");
        $__internal_a76299bd463d5a30a10291bfb35387049a53d410c8f7861aa92f330e4c9c3d0d->enter($__internal_a76299bd463d5a30a10291bfb35387049a53d410c8f7861aa92f330e4c9c3d0d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/email_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'email')) ?>
";
        
        $__internal_a76299bd463d5a30a10291bfb35387049a53d410c8f7861aa92f330e4c9c3d0d->leave($__internal_a76299bd463d5a30a10291bfb35387049a53d410c8f7861aa92f330e4c9c3d0d_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/email_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple', array('type' => isset($type) ? $type : 'email')) ?>*/
/* */
