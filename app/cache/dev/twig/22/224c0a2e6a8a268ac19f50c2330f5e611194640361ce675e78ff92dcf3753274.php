<?php

/* trip/new.html.twig */
class __TwigTemplate_74ce7929b12fc9217b2deba24c0ae9395929ce5a8b384fb8a6cf005a7dc690aa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "trip/new.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d0a11039d7de2be85e3349b33c4bfb538add4ad05ea141c594c2603deeb3cc3e = $this->env->getExtension("native_profiler");
        $__internal_d0a11039d7de2be85e3349b33c4bfb538add4ad05ea141c594c2603deeb3cc3e->enter($__internal_d0a11039d7de2be85e3349b33c4bfb538add4ad05ea141c594c2603deeb3cc3e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "trip/new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_d0a11039d7de2be85e3349b33c4bfb538add4ad05ea141c594c2603deeb3cc3e->leave($__internal_d0a11039d7de2be85e3349b33c4bfb538add4ad05ea141c594c2603deeb3cc3e_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_0703a6cc81680419f8ea1f9d08f1bc28888be0f5edde705dffe719bb8649357a = $this->env->getExtension("native_profiler");
        $__internal_0703a6cc81680419f8ea1f9d08f1bc28888be0f5edde705dffe719bb8649357a->enter($__internal_0703a6cc81680419f8ea1f9d08f1bc28888be0f5edde705dffe719bb8649357a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Trip creation</h1>


  <form class=\"form-horizontal\" method=\"post\" action=\"";
        // line 7
        echo $this->env->getExtension('routing')->getPath("trip_new");
        echo "\">
    <div class=\"form-group\">
        <label for=\"title\" class=\"col-sm-2 control-label\">NOMBRE</label>
        <div class=\"col-sm-10\">
            <input type=\"text\" class=\"form-control\" name=\"name\" id=\"title\" placeholder=\"Titulo\">
        </div>
    </div>
    <div class=\"form-group\">
        <label for=\"cover\" class=\"col-sm-2 control-label\">DESCRIPCION</label>
        <div class=\"col-sm-10\">
            <input type=\"text\" class=\"form-control\" id=\"cover\" name=\"description\" placeholder=\"URL de la portada\">
        </div>
    </div>
    <div class=\"form-group\">
        <label for=\"author\" class=\"col-sm-2 control-label\">URL IMAGEN</label>
        <div class=\"col-sm-10\">
            <input type=\"url\" class=\"form-control\" id=\"author\" name=\"url\" placeholder=\"Autor\">
        </div>
    </div>
    <div class=\"form-group\">
        <label for=\"price\" class=\"col-sm-2 control-label\">PRECIO</label>
        <div class=\"col-sm-10\">
            <input type=\"number\" class=\"form-control\" id=\"price\" name=\"price\" placeholder=\"Precio\">
        </div>
    </div>
    <div class=\"form-group\">
        <div class=\"col-sm-offset-2 col-sm-10\">
            <button type=\"submit\" class=\"btn btn-success pull-right\">Crear</button>
        </div>
    </div>
</form>
  

    <ul>
        <li>
            <a href=\"";
        // line 42
        echo $this->env->getExtension('routing')->getPath("trip_index");
        echo "\">Back to the list</a>
        </li>
    </ul>
";
        
        $__internal_0703a6cc81680419f8ea1f9d08f1bc28888be0f5edde705dffe719bb8649357a->leave($__internal_0703a6cc81680419f8ea1f9d08f1bc28888be0f5edde705dffe719bb8649357a_prof);

    }

    public function getTemplateName()
    {
        return "trip/new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  83 => 42,  45 => 7,  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends 'base.html.twig' %}*/
/* */
/* {% block body %}*/
/*     <h1>Trip creation</h1>*/
/* */
/* */
/*   <form class="form-horizontal" method="post" action="{{ path('trip_new') }}">*/
/*     <div class="form-group">*/
/*         <label for="title" class="col-sm-2 control-label">NOMBRE</label>*/
/*         <div class="col-sm-10">*/
/*             <input type="text" class="form-control" name="name" id="title" placeholder="Titulo">*/
/*         </div>*/
/*     </div>*/
/*     <div class="form-group">*/
/*         <label for="cover" class="col-sm-2 control-label">DESCRIPCION</label>*/
/*         <div class="col-sm-10">*/
/*             <input type="text" class="form-control" id="cover" name="description" placeholder="URL de la portada">*/
/*         </div>*/
/*     </div>*/
/*     <div class="form-group">*/
/*         <label for="author" class="col-sm-2 control-label">URL IMAGEN</label>*/
/*         <div class="col-sm-10">*/
/*             <input type="url" class="form-control" id="author" name="url" placeholder="Autor">*/
/*         </div>*/
/*     </div>*/
/*     <div class="form-group">*/
/*         <label for="price" class="col-sm-2 control-label">PRECIO</label>*/
/*         <div class="col-sm-10">*/
/*             <input type="number" class="form-control" id="price" name="price" placeholder="Precio">*/
/*         </div>*/
/*     </div>*/
/*     <div class="form-group">*/
/*         <div class="col-sm-offset-2 col-sm-10">*/
/*             <button type="submit" class="btn btn-success pull-right">Crear</button>*/
/*         </div>*/
/*     </div>*/
/* </form>*/
/*   */
/* */
/*     <ul>*/
/*         <li>*/
/*             <a href="{{ path('trip_index') }}">Back to the list</a>*/
/*         </li>*/
/*     </ul>*/
/* {% endblock %}*/
/* */
