<?php

/* @Framework/FormTable/form_widget_compound.html.php */
class __TwigTemplate_a8cd297a3b176bb5b61e23d8be1e7f0d9904bbbd8058e9510546048aeb7a901f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_471f05f3e8af5b7f5d1a3828f1bd0d301fa4366257c6cedfeb05275ac44f2005 = $this->env->getExtension("native_profiler");
        $__internal_471f05f3e8af5b7f5d1a3828f1bd0d301fa4366257c6cedfeb05275ac44f2005->enter($__internal_471f05f3e8af5b7f5d1a3828f1bd0d301fa4366257c6cedfeb05275ac44f2005_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_widget_compound.html.php"));

        // line 1
        echo "<table <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <tr>
        <td colspan=\"2\">
            <?php echo \$view['form']->errors(\$form) ?>
        </td>
    </tr>
    <?php endif ?>
    <?php echo \$view['form']->block(\$form, 'form_rows') ?>
    <?php echo \$view['form']->rest(\$form) ?>
</table>
";
        
        $__internal_471f05f3e8af5b7f5d1a3828f1bd0d301fa4366257c6cedfeb05275ac44f2005->leave($__internal_471f05f3e8af5b7f5d1a3828f1bd0d301fa4366257c6cedfeb05275ac44f2005_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/form_widget_compound.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <table <?php echo $view['form']->block($form, 'widget_container_attributes') ?>>*/
/*     <?php if (!$form->parent && $errors): ?>*/
/*     <tr>*/
/*         <td colspan="2">*/
/*             <?php echo $view['form']->errors($form) ?>*/
/*         </td>*/
/*     </tr>*/
/*     <?php endif ?>*/
/*     <?php echo $view['form']->block($form, 'form_rows') ?>*/
/*     <?php echo $view['form']->rest($form) ?>*/
/* </table>*/
/* */
