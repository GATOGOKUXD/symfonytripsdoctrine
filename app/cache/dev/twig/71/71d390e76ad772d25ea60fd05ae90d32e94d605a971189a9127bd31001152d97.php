<?php

/* @Twig/Exception/exception.rdf.twig */
class __TwigTemplate_2a04e5aeb5d5cc45d67c2e4782857db44e7d3252a480abad43c23edc82bdd3f9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e4e8f461c3371ec56a7e13368a810d2a7faeda29405be4f85b4cfac238e207e8 = $this->env->getExtension("native_profiler");
        $__internal_e4e8f461c3371ec56a7e13368a810d2a7faeda29405be4f85b4cfac238e207e8->enter($__internal_e4e8f461c3371ec56a7e13368a810d2a7faeda29405be4f85b4cfac238e207e8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception.rdf.twig"));

        // line 1
        $this->loadTemplate("@Twig/Exception/exception.xml.twig", "@Twig/Exception/exception.rdf.twig", 1)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        
        $__internal_e4e8f461c3371ec56a7e13368a810d2a7faeda29405be4f85b4cfac238e207e8->leave($__internal_e4e8f461c3371ec56a7e13368a810d2a7faeda29405be4f85b4cfac238e207e8_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception.rdf.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {% include '@Twig/Exception/exception.xml.twig' with { 'exception': exception } %}*/
/* */
