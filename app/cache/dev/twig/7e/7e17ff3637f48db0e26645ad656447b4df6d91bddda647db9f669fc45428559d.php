<?php

/* trip/index.html.twig */
class __TwigTemplate_4141cdc125cd57e1d13fbf98f79f3c56466e3282f3a94b954a177700b232aba6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "trip/index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_32ff4088dcaade9abefaf2731ee566c6eea6de5a107cbd8a78ebc900fa816c97 = $this->env->getExtension("native_profiler");
        $__internal_32ff4088dcaade9abefaf2731ee566c6eea6de5a107cbd8a78ebc900fa816c97->enter($__internal_32ff4088dcaade9abefaf2731ee566c6eea6de5a107cbd8a78ebc900fa816c97_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "trip/index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_32ff4088dcaade9abefaf2731ee566c6eea6de5a107cbd8a78ebc900fa816c97->leave($__internal_32ff4088dcaade9abefaf2731ee566c6eea6de5a107cbd8a78ebc900fa816c97_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_cd3b8566172829938785c868297987a9b35b56a11645045a3ba7fccd341588a7 = $this->env->getExtension("native_profiler");
        $__internal_cd3b8566172829938785c868297987a9b35b56a11645045a3ba7fccd341588a7->enter($__internal_cd3b8566172829938785c868297987a9b35b56a11645045a3ba7fccd341588a7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    
    <h1>Trip list</h1>
        ";
        // line 6
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["trips"]) ? $context["trips"] : $this->getContext($context, "trips")));
        foreach ($context['_seq'] as $context["_key"] => $context["trip"]) {
            // line 7
            echo "        
            <div class=\"col-md-4\">
                <table class=\"table-responsive\">
                    <tr> <td>      ID:    <a href=\"";
            // line 10
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("trip_show", array("id" => $this->getAttribute($context["trip"], "id", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["trip"], "id", array()), "html", null, true);
            echo "</a>   </td> </tr>
                         <tr> <td>   NAME: <h2>";
            // line 11
            echo twig_escape_filter($this->env, $this->getAttribute($context["trip"], "name", array()), "html", null, true);
            echo "</h2>    </td> </tr>
                              <tr> <td>   DSCRIPTION:   <p>";
            // line 12
            echo twig_escape_filter($this->env, $this->getAttribute($context["trip"], "description", array()), "html", null, true);
            echo "</p>   </td> </tr>
                                   <tr> <td>    PRICE:       <h4>";
            // line 13
            echo twig_escape_filter($this->env, $this->getAttribute($context["trip"], "price", array()), "html", null, true);
            echo "</h4>   </td> </tr>
                         
                </table>
     
             
                <image class=\"img-circle\"  width=\"304\" height=\"236\"  src=\"";
            // line 18
            echo twig_escape_filter($this->env, $this->getAttribute($context["trip"], "urlPicture", array()), "html", null, true);
            echo "\" />
        
          
                   <div class=\"btn-group-justified\">
                            <a class=\"btn btn-default\"  href=\"";
            // line 22
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("trip_show", array("id" => $this->getAttribute($context["trip"], "id", array()))), "html", null, true);
            echo "\">show</a>
                            <a class=\"btn btn-info\"  href=\"";
            // line 23
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("trip_edit", array("id" => $this->getAttribute($context["trip"], "id", array()))), "html", null, true);
            echo "\">edit</a>
                          
                            <a class=\"btn btn-danger\"  href=\"";
            // line 25
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("trip_delete", array("id" => $this->getAttribute($context["trip"], "id", array()))), "html", null, true);
            echo "\">delete</a>
                            </div>
                </div>
                           
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['trip'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 30
        echo "
        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"";
        // line 36
        echo $this->env->getExtension('routing')->getPath("create_action");
        echo "\">Create a new entry</a>
        </li>
    </ul>
";
        
        $__internal_cd3b8566172829938785c868297987a9b35b56a11645045a3ba7fccd341588a7->leave($__internal_cd3b8566172829938785c868297987a9b35b56a11645045a3ba7fccd341588a7_prof);

    }

    public function getTemplateName()
    {
        return "trip/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  110 => 36,  102 => 30,  91 => 25,  86 => 23,  82 => 22,  75 => 18,  67 => 13,  63 => 12,  59 => 11,  53 => 10,  48 => 7,  44 => 6,  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends 'base.html.twig' %}*/
/* */
/* {% block body %}*/
/*     */
/*     <h1>Trip list</h1>*/
/*         {% for trip in trips %}*/
/*         */
/*             <div class="col-md-4">*/
/*                 <table class="table-responsive">*/
/*                     <tr> <td>      ID:    <a href="{{ path('trip_show', { 'id': trip.id }) }}">{{ trip.id }}</a>   </td> </tr>*/
/*                          <tr> <td>   NAME: <h2>{{ trip.name }}</h2>    </td> </tr>*/
/*                               <tr> <td>   DSCRIPTION:   <p>{{ trip.description }}</p>   </td> </tr>*/
/*                                    <tr> <td>    PRICE:       <h4>{{ trip.price }}</h4>   </td> </tr>*/
/*                          */
/*                 </table>*/
/*      */
/*              */
/*                 <image class="img-circle"  width="304" height="236"  src="{{ trip.urlPicture }}" />*/
/*         */
/*           */
/*                    <div class="btn-group-justified">*/
/*                             <a class="btn btn-default"  href="{{ path('trip_show', { 'id': trip.id }) }}">show</a>*/
/*                             <a class="btn btn-info"  href="{{ path('trip_edit', { 'id': trip.id }) }}">edit</a>*/
/*                           */
/*                             <a class="btn btn-danger"  href="{{ path('trip_delete', { 'id': trip.id }) }}">delete</a>*/
/*                             </div>*/
/*                 </div>*/
/*                            */
/*         {% endfor %}*/
/* */
/*         </tbody>*/
/*     </table>*/
/* */
/*     <ul>*/
/*         <li>*/
/*             <a href="{{ path('create_action') }}">Create a new entry</a>*/
/*         </li>*/
/*     </ul>*/
/* {% endblock %}*/
/* */
