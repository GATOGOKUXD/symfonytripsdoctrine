<?php

/* @Framework/Form/form_errors.html.php */
class __TwigTemplate_30da046dd37e1173ff38cc40a8acadf2745a934e85d560736ae1aa78f02ff1fe extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_aa427a3a68d79eee6717d280e46ea6c1ef3dbf990294d1a2a7b8aaf1d746c460 = $this->env->getExtension("native_profiler");
        $__internal_aa427a3a68d79eee6717d280e46ea6c1ef3dbf990294d1a2a7b8aaf1d746c460->enter($__internal_aa427a3a68d79eee6717d280e46ea6c1ef3dbf990294d1a2a7b8aaf1d746c460_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_errors.html.php"));

        // line 1
        echo "<?php if (count(\$errors) > 0): ?>
    <ul>
        <?php foreach (\$errors as \$error): ?>
            <li><?php echo \$error->getMessage() ?></li>
        <?php endforeach; ?>
    </ul>
<?php endif ?>
";
        
        $__internal_aa427a3a68d79eee6717d280e46ea6c1ef3dbf990294d1a2a7b8aaf1d746c460->leave($__internal_aa427a3a68d79eee6717d280e46ea6c1ef3dbf990294d1a2a7b8aaf1d746c460_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_errors.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if (count($errors) > 0): ?>*/
/*     <ul>*/
/*         <?php foreach ($errors as $error): ?>*/
/*             <li><?php echo $error->getMessage() ?></li>*/
/*         <?php endforeach; ?>*/
/*     </ul>*/
/* <?php endif ?>*/
/* */
