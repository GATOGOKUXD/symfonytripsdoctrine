<?php

/* @Framework/Form/form.html.php */
class __TwigTemplate_2a941982abe352bdb1885aa1a35cdefcdb97f60549dfa831b4f80159073cef58 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_dfbed72bd1c3bb18e89538ca2cee3bc5510aa0b27bd6b09acf2f0bd58f536b9e = $this->env->getExtension("native_profiler");
        $__internal_dfbed72bd1c3bb18e89538ca2cee3bc5510aa0b27bd6b09acf2f0bd58f536b9e->enter($__internal_dfbed72bd1c3bb18e89538ca2cee3bc5510aa0b27bd6b09acf2f0bd58f536b9e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form.html.php"));

        // line 1
        echo "<?php echo \$view['form']->start(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
<?php echo \$view['form']->end(\$form) ?>
";
        
        $__internal_dfbed72bd1c3bb18e89538ca2cee3bc5510aa0b27bd6b09acf2f0bd58f536b9e->leave($__internal_dfbed72bd1c3bb18e89538ca2cee3bc5510aa0b27bd6b09acf2f0bd58f536b9e_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->start($form) ?>*/
/*     <?php echo $view['form']->widget($form) ?>*/
/* <?php echo $view['form']->end($form) ?>*/
/* */
