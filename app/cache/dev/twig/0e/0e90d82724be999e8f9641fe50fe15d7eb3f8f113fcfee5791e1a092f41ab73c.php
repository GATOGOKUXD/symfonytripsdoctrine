<?php

/* TwigBundle:Exception:error.atom.twig */
class __TwigTemplate_425a56c895773ec9971659bca7ef57c66ef70d948e2cdea101b6bf365fc11ca0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_75650f6b6bd63800f7e7dbac7578cb2d410c3b606730dfc1192ef4e4a5a15b91 = $this->env->getExtension("native_profiler");
        $__internal_75650f6b6bd63800f7e7dbac7578cb2d410c3b606730dfc1192ef4e4a5a15b91->enter($__internal_75650f6b6bd63800f7e7dbac7578cb2d410c3b606730dfc1192ef4e4a5a15b91_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.atom.twig"));

        // line 1
        $this->loadTemplate("@Twig/Exception/error.xml.twig", "TwigBundle:Exception:error.atom.twig", 1)->display($context);
        
        $__internal_75650f6b6bd63800f7e7dbac7578cb2d410c3b606730dfc1192ef4e4a5a15b91->leave($__internal_75650f6b6bd63800f7e7dbac7578cb2d410c3b606730dfc1192ef4e4a5a15b91_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.atom.twig";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {% include '@Twig/Exception/error.xml.twig' %}*/
/* */
