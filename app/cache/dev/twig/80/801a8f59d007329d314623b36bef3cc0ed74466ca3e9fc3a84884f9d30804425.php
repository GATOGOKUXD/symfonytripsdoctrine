<?php

/* @Twig/Exception/error.json.twig */
class __TwigTemplate_cff5e5a125e7bb19de2706a53fe935a62cb57f69eeb71790945182a80987faf7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bcec994ab9289d08679b2910364c4d0a7d4deeec534dd23a16074c6b73ce838f = $this->env->getExtension("native_profiler");
        $__internal_bcec994ab9289d08679b2910364c4d0a7d4deeec534dd23a16074c6b73ce838f->enter($__internal_bcec994ab9289d08679b2910364c4d0a7d4deeec534dd23a16074c6b73ce838f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.json.twig"));

        // line 1
        echo twig_jsonencode_filter(array("error" => array("code" => (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "message" => (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")))));
        echo "
";
        
        $__internal_bcec994ab9289d08679b2910364c4d0a7d4deeec534dd23a16074c6b73ce838f->leave($__internal_bcec994ab9289d08679b2910364c4d0a7d4deeec534dd23a16074c6b73ce838f_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/error.json.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {{ { 'error': { 'code': status_code, 'message': status_text } }|json_encode|raw }}*/
/* */
