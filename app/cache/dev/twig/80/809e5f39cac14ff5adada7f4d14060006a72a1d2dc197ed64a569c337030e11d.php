<?php

/* @Framework/Form/attributes.html.php */
class __TwigTemplate_5964e2c456126c20e82426cbdcebd33b83c5267a35a2cc14560fbda317bd59fb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c0ee9d3a835640e3d23f28cc15d986b4b436142984fff917bf03c079fcfd2142 = $this->env->getExtension("native_profiler");
        $__internal_c0ee9d3a835640e3d23f28cc15d986b4b436142984fff917bf03c079fcfd2142->enter($__internal_c0ee9d3a835640e3d23f28cc15d986b4b436142984fff917bf03c079fcfd2142_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/attributes.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
";
        
        $__internal_c0ee9d3a835640e3d23f28cc15d986b4b436142984fff917bf03c079fcfd2142->leave($__internal_c0ee9d3a835640e3d23f28cc15d986b4b436142984fff917bf03c079fcfd2142_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'widget_attributes') ?>*/
/* */
