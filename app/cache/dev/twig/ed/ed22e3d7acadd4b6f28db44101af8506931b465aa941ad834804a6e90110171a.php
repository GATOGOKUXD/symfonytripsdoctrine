<?php

/* @Framework/Form/form_rest.html.php */
class __TwigTemplate_760d4459acb567c60539cc573a664ecf753215e4ec730eb2399dd8ff3a1e2a36 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_aeed4b2fc04dd5afc9bc831e5a99fce764f4524c9c431e4f540d0c406ab111cc = $this->env->getExtension("native_profiler");
        $__internal_aeed4b2fc04dd5afc9bc831e5a99fce764f4524c9c431e4f540d0c406ab111cc->enter($__internal_aeed4b2fc04dd5afc9bc831e5a99fce764f4524c9c431e4f540d0c406ab111cc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rest.html.php"));

        // line 1
        echo "<?php foreach (\$form as \$child): ?>
    <?php if (!\$child->isRendered()): ?>
        <?php echo \$view['form']->row(\$child) ?>
    <?php endif; ?>
<?php endforeach; ?>
";
        
        $__internal_aeed4b2fc04dd5afc9bc831e5a99fce764f4524c9c431e4f540d0c406ab111cc->leave($__internal_aeed4b2fc04dd5afc9bc831e5a99fce764f4524c9c431e4f540d0c406ab111cc_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_rest.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php foreach ($form as $child): ?>*/
/*     <?php if (!$child->isRendered()): ?>*/
/*         <?php echo $view['form']->row($child) ?>*/
/*     <?php endif; ?>*/
/* <?php endforeach; ?>*/
/* */
