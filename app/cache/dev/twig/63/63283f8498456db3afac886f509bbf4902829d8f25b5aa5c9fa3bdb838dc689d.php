<?php

/* @Twig/Exception/error.atom.twig */
class __TwigTemplate_63018e01df34ac64de3ab96e0258213fab85977502050ab7d4344075d281f8d4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ef3b95183cb7d86dd2a8b4fe2dfcbb449fbd32c6542a503711ed1731f1c6bfcc = $this->env->getExtension("native_profiler");
        $__internal_ef3b95183cb7d86dd2a8b4fe2dfcbb449fbd32c6542a503711ed1731f1c6bfcc->enter($__internal_ef3b95183cb7d86dd2a8b4fe2dfcbb449fbd32c6542a503711ed1731f1c6bfcc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.atom.twig"));

        // line 1
        $this->loadTemplate("@Twig/Exception/error.xml.twig", "@Twig/Exception/error.atom.twig", 1)->display($context);
        
        $__internal_ef3b95183cb7d86dd2a8b4fe2dfcbb449fbd32c6542a503711ed1731f1c6bfcc->leave($__internal_ef3b95183cb7d86dd2a8b4fe2dfcbb449fbd32c6542a503711ed1731f1c6bfcc_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/error.atom.twig";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {% include '@Twig/Exception/error.xml.twig' %}*/
/* */
