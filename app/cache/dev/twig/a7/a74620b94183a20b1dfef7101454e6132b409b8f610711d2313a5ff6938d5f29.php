<?php

/* @WebProfiler/Profiler/toolbar_redirect.html.twig */
class __TwigTemplate_7cfcee4f3499b566cbb762ec2ac6deaa65ca2270c8467a8bad3d74f737b3c604 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@WebProfiler/Profiler/toolbar_redirect.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_85215b7a84b31274b4a084c72dccb1bdd559453637634c9c09dcd6795e4bfd3c = $this->env->getExtension("native_profiler");
        $__internal_85215b7a84b31274b4a084c72dccb1bdd559453637634c9c09dcd6795e4bfd3c->enter($__internal_85215b7a84b31274b4a084c72dccb1bdd559453637634c9c09dcd6795e4bfd3c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/toolbar_redirect.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_85215b7a84b31274b4a084c72dccb1bdd559453637634c9c09dcd6795e4bfd3c->leave($__internal_85215b7a84b31274b4a084c72dccb1bdd559453637634c9c09dcd6795e4bfd3c_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_45bee9b1f146b0e7e849350a3178b49004b48f680e31e3ddebd14367bc29f0a4 = $this->env->getExtension("native_profiler");
        $__internal_45bee9b1f146b0e7e849350a3178b49004b48f680e31e3ddebd14367bc29f0a4->enter($__internal_45bee9b1f146b0e7e849350a3178b49004b48f680e31e3ddebd14367bc29f0a4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Redirection Intercepted";
        
        $__internal_45bee9b1f146b0e7e849350a3178b49004b48f680e31e3ddebd14367bc29f0a4->leave($__internal_45bee9b1f146b0e7e849350a3178b49004b48f680e31e3ddebd14367bc29f0a4_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_2dc325ee16bb7b70f8015ad543c664390baa19c845f2713e9fe6b11242149d39 = $this->env->getExtension("native_profiler");
        $__internal_2dc325ee16bb7b70f8015ad543c664390baa19c845f2713e9fe6b11242149d39->enter($__internal_2dc325ee16bb7b70f8015ad543c664390baa19c845f2713e9fe6b11242149d39_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"";
        // line 8
        echo twig_escape_filter($this->env, (isset($context["location"]) ? $context["location"] : $this->getContext($context, "location")), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, (isset($context["location"]) ? $context["location"] : $this->getContext($context, "location")), "html", null, true);
        echo "</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
";
        
        $__internal_2dc325ee16bb7b70f8015ad543c664390baa19c845f2713e9fe6b11242149d39->leave($__internal_2dc325ee16bb7b70f8015ad543c664390baa19c845f2713e9fe6b11242149d39_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Profiler/toolbar_redirect.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 8,  53 => 6,  47 => 5,  35 => 3,  11 => 1,);
    }
}
/* {% extends '@Twig/layout.html.twig' %}*/
/* */
/* {% block title 'Redirection Intercepted' %}*/
/* */
/* {% block body %}*/
/*     <div class="sf-reset">*/
/*         <div class="block-exception">*/
/*             <h1>This request redirects to <a href="{{ location }}">{{ location }}</a>.</h1>*/
/* */
/*             <p>*/
/*                 <small>*/
/*                     The redirect was intercepted by the web debug toolbar to help debugging.*/
/*                     For more information, see the "intercept-redirects" option of the Profiler.*/
/*                 </small>*/
/*             </p>*/
/*         </div>*/
/*     </div>*/
/* {% endblock %}*/
/* */
