<?php

/* @WebProfiler/Profiler/ajax_layout.html.twig */
class __TwigTemplate_1227244fa268f073d484b9242f71155c468124bbe2d2503a6fdde6f6350690df extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6ae7a86b1fa3313590b46f321a90cc71cb6c6ee20bf330778a0fa749e01b2560 = $this->env->getExtension("native_profiler");
        $__internal_6ae7a86b1fa3313590b46f321a90cc71cb6c6ee20bf330778a0fa749e01b2560->enter($__internal_6ae7a86b1fa3313590b46f321a90cc71cb6c6ee20bf330778a0fa749e01b2560_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/ajax_layout.html.twig"));

        // line 1
        $this->displayBlock('panel', $context, $blocks);
        
        $__internal_6ae7a86b1fa3313590b46f321a90cc71cb6c6ee20bf330778a0fa749e01b2560->leave($__internal_6ae7a86b1fa3313590b46f321a90cc71cb6c6ee20bf330778a0fa749e01b2560_prof);

    }

    public function block_panel($context, array $blocks = array())
    {
        $__internal_e4f6d72a198944f13c48ac4ab5367ce35d911008f638aa58fce225ea97725c8e = $this->env->getExtension("native_profiler");
        $__internal_e4f6d72a198944f13c48ac4ab5367ce35d911008f638aa58fce225ea97725c8e->enter($__internal_e4f6d72a198944f13c48ac4ab5367ce35d911008f638aa58fce225ea97725c8e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        echo "";
        
        $__internal_e4f6d72a198944f13c48ac4ab5367ce35d911008f638aa58fce225ea97725c8e->leave($__internal_e4f6d72a198944f13c48ac4ab5367ce35d911008f638aa58fce225ea97725c8e_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Profiler/ajax_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  23 => 1,);
    }
}
/* {% block panel '' %}*/
/* */
