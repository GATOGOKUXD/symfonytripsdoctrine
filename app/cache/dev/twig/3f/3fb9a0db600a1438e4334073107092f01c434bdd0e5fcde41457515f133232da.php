<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_554d3fcca9e2a320a5f592fcac381b4be627aa0ea493b243ec0842ad5802d626 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7399743fc347e36756c159762c11b28f4e532dfa7f7d72d56247728a197e0e01 = $this->env->getExtension("native_profiler");
        $__internal_7399743fc347e36756c159762c11b28f4e532dfa7f7d72d56247728a197e0e01->enter($__internal_7399743fc347e36756c159762c11b28f4e532dfa7f7d72d56247728a197e0e01_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_7399743fc347e36756c159762c11b28f4e532dfa7f7d72d56247728a197e0e01->leave($__internal_7399743fc347e36756c159762c11b28f4e532dfa7f7d72d56247728a197e0e01_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_d95ee744da309e0db18b2a78acb62ce4d02d49aad23041875943b6686f997fb6 = $this->env->getExtension("native_profiler");
        $__internal_d95ee744da309e0db18b2a78acb62ce4d02d49aad23041875943b6686f997fb6->enter($__internal_d95ee744da309e0db18b2a78acb62ce4d02d49aad23041875943b6686f997fb6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_d95ee744da309e0db18b2a78acb62ce4d02d49aad23041875943b6686f997fb6->leave($__internal_d95ee744da309e0db18b2a78acb62ce4d02d49aad23041875943b6686f997fb6_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_21fb49a55d1eec3f34cc2574571f15b63716845a4a9db1694499ad9d969a84fc = $this->env->getExtension("native_profiler");
        $__internal_21fb49a55d1eec3f34cc2574571f15b63716845a4a9db1694499ad9d969a84fc->enter($__internal_21fb49a55d1eec3f34cc2574571f15b63716845a4a9db1694499ad9d969a84fc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_21fb49a55d1eec3f34cc2574571f15b63716845a4a9db1694499ad9d969a84fc->leave($__internal_21fb49a55d1eec3f34cc2574571f15b63716845a4a9db1694499ad9d969a84fc_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_86b27ce6c06addf0e149ea24a06d891bb4d25d65bd3134c06895996a94b99b11 = $this->env->getExtension("native_profiler");
        $__internal_86b27ce6c06addf0e149ea24a06d891bb4d25d65bd3134c06895996a94b99b11->enter($__internal_86b27ce6c06addf0e149ea24a06d891bb4d25d65bd3134c06895996a94b99b11_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('routing')->getPath("_profiler_router", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_86b27ce6c06addf0e149ea24a06d891bb4d25d65bd3134c06895996a94b99b11->leave($__internal_86b27ce6c06addf0e149ea24a06d891bb4d25d65bd3134c06895996a94b99b11_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 13,  67 => 12,  56 => 7,  53 => 6,  47 => 5,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@WebProfiler/Profiler/layout.html.twig' %}*/
/* */
/* {% block toolbar %}{% endblock %}*/
/* */
/* {% block menu %}*/
/* <span class="label">*/
/*     <span class="icon">{{ include('@WebProfiler/Icon/router.svg') }}</span>*/
/*     <strong>Routing</strong>*/
/* </span>*/
/* {% endblock %}*/
/* */
/* {% block panel %}*/
/*     {{ render(path('_profiler_router', { token: token })) }}*/
/* {% endblock %}*/
/* */
