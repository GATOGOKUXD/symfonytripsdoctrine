<?php

/* @Twig/Exception/exception.atom.twig */
class __TwigTemplate_b527bc3ee94ccc8ee11d1c47ca5df7ec855206bb41102746230a1819cb2c86e0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3ca92617a8c25850c9850c4eda255134df2f4c84ff9e0e60cf305d89450b26c4 = $this->env->getExtension("native_profiler");
        $__internal_3ca92617a8c25850c9850c4eda255134df2f4c84ff9e0e60cf305d89450b26c4->enter($__internal_3ca92617a8c25850c9850c4eda255134df2f4c84ff9e0e60cf305d89450b26c4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception.atom.twig"));

        // line 1
        $this->loadTemplate("@Twig/Exception/exception.xml.twig", "@Twig/Exception/exception.atom.twig", 1)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        
        $__internal_3ca92617a8c25850c9850c4eda255134df2f4c84ff9e0e60cf305d89450b26c4->leave($__internal_3ca92617a8c25850c9850c4eda255134df2f4c84ff9e0e60cf305d89450b26c4_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception.atom.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {% include '@Twig/Exception/exception.xml.twig' with { 'exception': exception } %}*/
/* */
