<?php

/* @Twig/Exception/exception.json.twig */
class __TwigTemplate_5bbb90b0ee26633c80125dfe6058ed6651af3b88c49d23f5c575be8dbf78f654 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0c03c1b7b79c4f33b9ceaa773216c0fd6c61dc8e8fcb3cd3aa2c6d33d0da7cef = $this->env->getExtension("native_profiler");
        $__internal_0c03c1b7b79c4f33b9ceaa773216c0fd6c61dc8e8fcb3cd3aa2c6d33d0da7cef->enter($__internal_0c03c1b7b79c4f33b9ceaa773216c0fd6c61dc8e8fcb3cd3aa2c6d33d0da7cef_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception.json.twig"));

        // line 1
        echo twig_jsonencode_filter(array("error" => array("code" => (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "message" => (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "exception" => $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "toarray", array()))));
        echo "
";
        
        $__internal_0c03c1b7b79c4f33b9ceaa773216c0fd6c61dc8e8fcb3cd3aa2c6d33d0da7cef->leave($__internal_0c03c1b7b79c4f33b9ceaa773216c0fd6c61dc8e8fcb3cd3aa2c6d33d0da7cef_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception.json.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {{ { 'error': { 'code': status_code, 'message': status_text, 'exception': exception.toarray } }|json_encode|raw }}*/
/* */
