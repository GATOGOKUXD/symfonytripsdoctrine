<?php

/* @Framework/Form/money_widget.html.php */
class __TwigTemplate_0acb6edd8675b21dee9f89d61ceb19ca52a0b9d9a07534bc51de2e2dd2e4c134 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c67132aaafecabc51c29ef3a9997ee972efe175918774ca8f5b6b1f680215499 = $this->env->getExtension("native_profiler");
        $__internal_c67132aaafecabc51c29ef3a9997ee972efe175918774ca8f5b6b1f680215499->enter($__internal_c67132aaafecabc51c29ef3a9997ee972efe175918774ca8f5b6b1f680215499_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/money_widget.html.php"));

        // line 1
        echo "<?php echo str_replace('";
        echo twig_escape_filter($this->env, (isset($context["widget"]) ? $context["widget"] : $this->getContext($context, "widget")), "html", null, true);
        echo "', \$view['form']->block(\$form, 'form_widget_simple'), \$money_pattern) ?>
";
        
        $__internal_c67132aaafecabc51c29ef3a9997ee972efe175918774ca8f5b6b1f680215499->leave($__internal_c67132aaafecabc51c29ef3a9997ee972efe175918774ca8f5b6b1f680215499_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/money_widget.html.php";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo str_replace('{{ widget }}', $view['form']->block($form, 'form_widget_simple'), $money_pattern) ?>*/
/* */
