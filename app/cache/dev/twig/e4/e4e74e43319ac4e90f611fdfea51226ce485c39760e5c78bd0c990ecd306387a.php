<?php

/* TwigBundle:Exception:exception.rdf.twig */
class __TwigTemplate_d8774e8c3727893064a4a8b6dfbf202831fe6ffcdb127b7058014fccc08c873b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6f3d0c6fe33c3b46df67ac004b29d91231eb6b5282a3ba9071423d2bce905907 = $this->env->getExtension("native_profiler");
        $__internal_6f3d0c6fe33c3b46df67ac004b29d91231eb6b5282a3ba9071423d2bce905907->enter($__internal_6f3d0c6fe33c3b46df67ac004b29d91231eb6b5282a3ba9071423d2bce905907_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.rdf.twig"));

        // line 1
        $this->loadTemplate("@Twig/Exception/exception.xml.twig", "TwigBundle:Exception:exception.rdf.twig", 1)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        
        $__internal_6f3d0c6fe33c3b46df67ac004b29d91231eb6b5282a3ba9071423d2bce905907->leave($__internal_6f3d0c6fe33c3b46df67ac004b29d91231eb6b5282a3ba9071423d2bce905907_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.rdf.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {% include '@Twig/Exception/exception.xml.twig' with { 'exception': exception } %}*/
/* */
