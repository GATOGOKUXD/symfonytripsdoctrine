<?php

/* @Framework/Form/form_widget.html.php */
class __TwigTemplate_d0e5b24df0b966033a83e38044bdcc7e07b1bd1c908a7684438f01732a2cdb59 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_58247fc884c89a1ce6d25eb5753054334bdc3c4acc943b47d8ba42b3e605f683 = $this->env->getExtension("native_profiler");
        $__internal_58247fc884c89a1ce6d25eb5753054334bdc3c4acc943b47d8ba42b3e605f683->enter($__internal_58247fc884c89a1ce6d25eb5753054334bdc3c4acc943b47d8ba42b3e605f683_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget.html.php"));

        // line 1
        echo "<?php if (\$compound): ?>
<?php echo \$view['form']->block(\$form, 'form_widget_compound')?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'form_widget_simple')?>
<?php endif ?>
";
        
        $__internal_58247fc884c89a1ce6d25eb5753054334bdc3c4acc943b47d8ba42b3e605f683->leave($__internal_58247fc884c89a1ce6d25eb5753054334bdc3c4acc943b47d8ba42b3e605f683_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if ($compound): ?>*/
/* <?php echo $view['form']->block($form, 'form_widget_compound')?>*/
/* <?php else: ?>*/
/* <?php echo $view['form']->block($form, 'form_widget_simple')?>*/
/* <?php endif ?>*/
/* */
