<?php

/* @Framework/Form/range_widget.html.php */
class __TwigTemplate_3546ad665e4e8c6b85af493567dd189a72304cf628f1e267ab7f61f804691fe9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0ce6baf04a054630fcd2f9021b8acdb04c21155c25570d1d98ee4bccee1786f2 = $this->env->getExtension("native_profiler");
        $__internal_0ce6baf04a054630fcd2f9021b8acdb04c21155c25570d1d98ee4bccee1786f2->enter($__internal_0ce6baf04a054630fcd2f9021b8acdb04c21155c25570d1d98ee4bccee1786f2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/range_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'range'));
";
        
        $__internal_0ce6baf04a054630fcd2f9021b8acdb04c21155c25570d1d98ee4bccee1786f2->leave($__internal_0ce6baf04a054630fcd2f9021b8acdb04c21155c25570d1d98ee4bccee1786f2_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/range_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple', array('type' => isset($type) ? $type : 'range'));*/
/* */
