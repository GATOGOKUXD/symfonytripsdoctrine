<?php

/* @Framework/Form/checkbox_widget.html.php */
class __TwigTemplate_86a6676004ae6f2f3c781aa5ab06db5c2e0644416faeff08ea0ae2b7e334bcd7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4463b78a0a4a3de72f7c578f1e2a9bf6e1cdb437211946dffef7a0898437c067 = $this->env->getExtension("native_profiler");
        $__internal_4463b78a0a4a3de72f7c578f1e2a9bf6e1cdb437211946dffef7a0898437c067->enter($__internal_4463b78a0a4a3de72f7c578f1e2a9bf6e1cdb437211946dffef7a0898437c067_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/checkbox_widget.html.php"));

        // line 1
        echo "<input type=\"checkbox\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    <?php if (strlen(\$value) > 0): ?> value=\"<?php echo \$view->escape(\$value) ?>\"<?php endif ?>
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
";
        
        $__internal_4463b78a0a4a3de72f7c578f1e2a9bf6e1cdb437211946dffef7a0898437c067->leave($__internal_4463b78a0a4a3de72f7c578f1e2a9bf6e1cdb437211946dffef7a0898437c067_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/checkbox_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <input type="checkbox"*/
/*     <?php echo $view['form']->block($form, 'widget_attributes') ?>*/
/*     <?php if (strlen($value) > 0): ?> value="<?php echo $view->escape($value) ?>"<?php endif ?>*/
/*     <?php if ($checked): ?> checked="checked"<?php endif ?>*/
/* />*/
/* */
