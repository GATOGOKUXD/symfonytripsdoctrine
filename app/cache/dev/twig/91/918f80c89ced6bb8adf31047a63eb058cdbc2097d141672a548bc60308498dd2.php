<?php

/* @Framework/Form/choice_widget_expanded.html.php */
class __TwigTemplate_9ddcba0eed1da2171cf986eb25c6cf02eed9c16f1c0597c5e6af513fdc6aa9ad extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0dc360ce741b6db3ce1ae49db1562399d8ace018867be1707ca15ae7a273be2d = $this->env->getExtension("native_profiler");
        $__internal_0dc360ce741b6db3ce1ae49db1562399d8ace018867be1707ca15ae7a273be2d->enter($__internal_0dc360ce741b6db3ce1ae49db1562399d8ace018867be1707ca15ae7a273be2d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget_expanded.html.php"));

        // line 1
        echo "<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
<?php foreach (\$form as \$child): ?>
    <?php echo \$view['form']->widget(\$child) ?>
    <?php echo \$view['form']->label(\$child, null, array('translation_domain' => \$choice_translation_domain)) ?>
<?php endforeach ?>
</div>
";
        
        $__internal_0dc360ce741b6db3ce1ae49db1562399d8ace018867be1707ca15ae7a273be2d->leave($__internal_0dc360ce741b6db3ce1ae49db1562399d8ace018867be1707ca15ae7a273be2d_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_widget_expanded.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <div <?php echo $view['form']->block($form, 'widget_container_attributes') ?>>*/
/* <?php foreach ($form as $child): ?>*/
/*     <?php echo $view['form']->widget($child) ?>*/
/*     <?php echo $view['form']->label($child, null, array('translation_domain' => $choice_translation_domain)) ?>*/
/* <?php endforeach ?>*/
/* </div>*/
/* */
