<?php

/* TwigBundle:Exception:exception.atom.twig */
class __TwigTemplate_c3229d1b2d6b873d9671320ec0686a663adce3b551454bb901225653e9bef177 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_abdc27dcbc088e1fdecff3caf0a79abda0dc13184c0d5c2e2c602d05fd94f8e9 = $this->env->getExtension("native_profiler");
        $__internal_abdc27dcbc088e1fdecff3caf0a79abda0dc13184c0d5c2e2c602d05fd94f8e9->enter($__internal_abdc27dcbc088e1fdecff3caf0a79abda0dc13184c0d5c2e2c602d05fd94f8e9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.atom.twig"));

        // line 1
        $this->loadTemplate("@Twig/Exception/exception.xml.twig", "TwigBundle:Exception:exception.atom.twig", 1)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        
        $__internal_abdc27dcbc088e1fdecff3caf0a79abda0dc13184c0d5c2e2c602d05fd94f8e9->leave($__internal_abdc27dcbc088e1fdecff3caf0a79abda0dc13184c0d5c2e2c602d05fd94f8e9_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.atom.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {% include '@Twig/Exception/exception.xml.twig' with { 'exception': exception } %}*/
/* */
