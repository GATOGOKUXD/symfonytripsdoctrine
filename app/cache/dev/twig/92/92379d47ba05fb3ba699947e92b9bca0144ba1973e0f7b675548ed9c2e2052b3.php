<?php

/* @Framework/Form/hidden_widget.html.php */
class __TwigTemplate_5bb6dc8f567a18f39e206a755e2b8b3bdee28250ebdb88a0ff457933422e9806 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b959b230a4072a5c29dfbd524b274fb6864c372ecb26bc7cf17022c9ef926e24 = $this->env->getExtension("native_profiler");
        $__internal_b959b230a4072a5c29dfbd524b274fb6864c372ecb26bc7cf17022c9ef926e24->enter($__internal_b959b230a4072a5c29dfbd524b274fb6864c372ecb26bc7cf17022c9ef926e24_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'hidden')) ?>
";
        
        $__internal_b959b230a4072a5c29dfbd524b274fb6864c372ecb26bc7cf17022c9ef926e24->leave($__internal_b959b230a4072a5c29dfbd524b274fb6864c372ecb26bc7cf17022c9ef926e24_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/hidden_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple', array('type' => isset($type) ? $type : 'hidden')) ?>*/
/* */
