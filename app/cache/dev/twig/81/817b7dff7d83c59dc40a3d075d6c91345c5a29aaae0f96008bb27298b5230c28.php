<?php

/* TwigBundle:Exception:error.json.twig */
class __TwigTemplate_699fc99e746c4f6d5f9ad88ad3c58925165b01a37c632983c4c6131301f42797 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0ad7c8d81cae4b2e22c4b2b93e955dfebe290917e9632734d5ab98ca82114b0e = $this->env->getExtension("native_profiler");
        $__internal_0ad7c8d81cae4b2e22c4b2b93e955dfebe290917e9632734d5ab98ca82114b0e->enter($__internal_0ad7c8d81cae4b2e22c4b2b93e955dfebe290917e9632734d5ab98ca82114b0e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.json.twig"));

        // line 1
        echo twig_jsonencode_filter(array("error" => array("code" => (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "message" => (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")))));
        echo "
";
        
        $__internal_0ad7c8d81cae4b2e22c4b2b93e955dfebe290917e9632734d5ab98ca82114b0e->leave($__internal_0ad7c8d81cae4b2e22c4b2b93e955dfebe290917e9632734d5ab98ca82114b0e_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.json.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {{ { 'error': { 'code': status_code, 'message': status_text } }|json_encode|raw }}*/
/* */
