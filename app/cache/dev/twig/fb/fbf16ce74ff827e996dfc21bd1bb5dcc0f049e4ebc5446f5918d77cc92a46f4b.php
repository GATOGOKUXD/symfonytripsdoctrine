<?php

/* @Security/Collector/icon.svg */
class __TwigTemplate_6e4386a9db1ef869e8e4c984a82e13e1dbbccd136eb8749eef52d6d55bf7672f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_eda9d5a1c4ba0e389b950374d5a95f26ae1de604c0fd04fa720bf5d048c48f39 = $this->env->getExtension("native_profiler");
        $__internal_eda9d5a1c4ba0e389b950374d5a95f26ae1de604c0fd04fa720bf5d048c48f39->enter($__internal_eda9d5a1c4ba0e389b950374d5a95f26ae1de604c0fd04fa720bf5d048c48f39_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Security/Collector/icon.svg"));

        // line 1
        echo "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
    <path fill=\"#AAAAAA\" d=\"M21,20.4V22H3v-1.6c0-3.7,2.4-6.9,5.8-8c-1.7-1.1-2.9-3-2.9-5.2c0-3.4,2.7-6.1,6.1-6.1s6.1,2.7,6.1,6.1c0,2.2-1.2,4.1-2.9,5.2C18.6,13.5,21,16.7,21,20.4z\"/>
</svg>
";
        
        $__internal_eda9d5a1c4ba0e389b950374d5a95f26ae1de604c0fd04fa720bf5d048c48f39->leave($__internal_eda9d5a1c4ba0e389b950374d5a95f26ae1de604c0fd04fa720bf5d048c48f39_prof);

    }

    public function getTemplateName()
    {
        return "@Security/Collector/icon.svg";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <svg version="1.1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" height="24" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve">*/
/*     <path fill="#AAAAAA" d="M21,20.4V22H3v-1.6c0-3.7,2.4-6.9,5.8-8c-1.7-1.1-2.9-3-2.9-5.2c0-3.4,2.7-6.1,6.1-6.1s6.1,2.7,6.1,6.1c0,2.2-1.2,4.1-2.9,5.2C18.6,13.5,21,16.7,21,20.4z"/>*/
/* </svg>*/
/* */
