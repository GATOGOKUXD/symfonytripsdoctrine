<?php

/* @Twig/Exception/error.css.twig */
class __TwigTemplate_09eadb4be3d74295ed3cb09516e2b147a1e62af0790b06f0337c3ac209d2f96b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_343c586c32dc8b4148a1b6ee7ad2283f2b07909cd57285555cbb5ce7c19ec010 = $this->env->getExtension("native_profiler");
        $__internal_343c586c32dc8b4148a1b6ee7ad2283f2b07909cd57285555cbb5ce7c19ec010->enter($__internal_343c586c32dc8b4148a1b6ee7ad2283f2b07909cd57285555cbb5ce7c19ec010_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.css.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "css", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "css", null, true);
        echo "

*/
";
        
        $__internal_343c586c32dc8b4148a1b6ee7ad2283f2b07909cd57285555cbb5ce7c19ec010->leave($__internal_343c586c32dc8b4148a1b6ee7ad2283f2b07909cd57285555cbb5ce7c19ec010_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/error.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 2,  22 => 1,);
    }
}
/* /**/
/* {{ status_code }} {{ status_text }}*/
/* */
/* *//* */
/* */
