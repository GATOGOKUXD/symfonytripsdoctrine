<?php

/* @Framework/FormTable/form_row.html.php */
class __TwigTemplate_f01cdc39049abdf50d108a782efea53df3001a609174c8580875c3c0a1dd099f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c35f17cbff5bda07ad9ea6d4b1ec641a99495c9443644f437dd1cc18e70e108f = $this->env->getExtension("native_profiler");
        $__internal_c35f17cbff5bda07ad9ea6d4b1ec641a99495c9443644f437dd1cc18e70e108f->enter($__internal_c35f17cbff5bda07ad9ea6d4b1ec641a99495c9443644f437dd1cc18e70e108f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_row.html.php"));

        // line 1
        echo "<tr>
    <td>
        <?php echo \$view['form']->label(\$form) ?>
    </td>
    <td>
        <?php echo \$view['form']->errors(\$form) ?>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $__internal_c35f17cbff5bda07ad9ea6d4b1ec641a99495c9443644f437dd1cc18e70e108f->leave($__internal_c35f17cbff5bda07ad9ea6d4b1ec641a99495c9443644f437dd1cc18e70e108f_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/form_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <tr>*/
/*     <td>*/
/*         <?php echo $view['form']->label($form) ?>*/
/*     </td>*/
/*     <td>*/
/*         <?php echo $view['form']->errors($form) ?>*/
/*         <?php echo $view['form']->widget($form) ?>*/
/*     </td>*/
/* </tr>*/
/* */
