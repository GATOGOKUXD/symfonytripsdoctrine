<?php

/* @Framework/Form/radio_widget.html.php */
class __TwigTemplate_95b767f6db0be30d698a86926b4d8cea39bc1f4abb8dcbae7e21d7c6306c71b2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c5a64134c9d62005936396ece722248d73c1d307719c9bb453052d5bde44b0ed = $this->env->getExtension("native_profiler");
        $__internal_c5a64134c9d62005936396ece722248d73c1d307719c9bb453052d5bde44b0ed->enter($__internal_c5a64134c9d62005936396ece722248d73c1d307719c9bb453052d5bde44b0ed_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/radio_widget.html.php"));

        // line 1
        echo "<input type=\"radio\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    value=\"<?php echo \$view->escape(\$value) ?>\"
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
";
        
        $__internal_c5a64134c9d62005936396ece722248d73c1d307719c9bb453052d5bde44b0ed->leave($__internal_c5a64134c9d62005936396ece722248d73c1d307719c9bb453052d5bde44b0ed_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/radio_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <input type="radio"*/
/*     <?php echo $view['form']->block($form, 'widget_attributes') ?>*/
/*     value="<?php echo $view->escape($value) ?>"*/
/*     <?php if ($checked): ?> checked="checked"<?php endif ?>*/
/* />*/
/* */
