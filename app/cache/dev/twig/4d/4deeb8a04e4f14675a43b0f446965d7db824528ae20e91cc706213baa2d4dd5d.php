<?php

/* @Framework/Form/datetime_widget.html.php */
class __TwigTemplate_78b8b1725ad51d0922ecece358b5fead50b8392fe880311e7750707bb65ba414 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_24b69b2b771aedef6337b5852083f40147b672b53047cbd864f2812d942b3a35 = $this->env->getExtension("native_profiler");
        $__internal_24b69b2b771aedef6337b5852083f40147b672b53047cbd864f2812d942b3a35->enter($__internal_24b69b2b771aedef6337b5852083f40147b672b53047cbd864f2812d942b3a35_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/datetime_widget.html.php"));

        // line 1
        echo "<?php if (\$widget == 'single_text'): ?>
    <?php echo \$view['form']->block(\$form, 'form_widget_simple'); ?>
<?php else: ?>
    <div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
        <?php echo \$view['form']->widget(\$form['date']).' '.\$view['form']->widget(\$form['time']) ?>
    </div>
<?php endif ?>
";
        
        $__internal_24b69b2b771aedef6337b5852083f40147b672b53047cbd864f2812d942b3a35->leave($__internal_24b69b2b771aedef6337b5852083f40147b672b53047cbd864f2812d942b3a35_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/datetime_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if ($widget == 'single_text'): ?>*/
/*     <?php echo $view['form']->block($form, 'form_widget_simple'); ?>*/
/* <?php else: ?>*/
/*     <div <?php echo $view['form']->block($form, 'widget_container_attributes') ?>>*/
/*         <?php echo $view['form']->widget($form['date']).' '.$view['form']->widget($form['time']) ?>*/
/*     </div>*/
/* <?php endif ?>*/
/* */
