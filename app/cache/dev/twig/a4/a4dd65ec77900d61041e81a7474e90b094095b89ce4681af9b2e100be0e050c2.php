<?php

/* @Framework/Form/integer_widget.html.php */
class __TwigTemplate_70559e875c47a3606706c717d7e70ebf03febf7dd918868fe41ca8afdf61ffdc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1320e1019d3f8ef46a3ddb5cf33a632d5b58731df8e8f307c23e6d2d8d903e9a = $this->env->getExtension("native_profiler");
        $__internal_1320e1019d3f8ef46a3ddb5cf33a632d5b58731df8e8f307c23e6d2d8d903e9a->enter($__internal_1320e1019d3f8ef46a3ddb5cf33a632d5b58731df8e8f307c23e6d2d8d903e9a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/integer_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'number')) ?>
";
        
        $__internal_1320e1019d3f8ef46a3ddb5cf33a632d5b58731df8e8f307c23e6d2d8d903e9a->leave($__internal_1320e1019d3f8ef46a3ddb5cf33a632d5b58731df8e8f307c23e6d2d8d903e9a_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/integer_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple', array('type' => isset($type) ? $type : 'number')) ?>*/
/* */
