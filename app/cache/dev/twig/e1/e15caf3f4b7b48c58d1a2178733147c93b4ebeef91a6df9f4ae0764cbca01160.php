<?php

/* @Framework/Form/submit_widget.html.php */
class __TwigTemplate_4fbcc7b4fd473c523f93a77d20f0ae7c94751733779ffe5ff2684181970e8c09 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_cb9f39aa18a21c8c5978c14f4b657e41b1eaba6ec544781b244424d327ed6f81 = $this->env->getExtension("native_profiler");
        $__internal_cb9f39aa18a21c8c5978c14f4b657e41b1eaba6ec544781b244424d327ed6f81->enter($__internal_cb9f39aa18a21c8c5978c14f4b657e41b1eaba6ec544781b244424d327ed6f81_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/submit_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'button_widget',  array('type' => isset(\$type) ? \$type : 'submit')) ?>
";
        
        $__internal_cb9f39aa18a21c8c5978c14f4b657e41b1eaba6ec544781b244424d327ed6f81->leave($__internal_cb9f39aa18a21c8c5978c14f4b657e41b1eaba6ec544781b244424d327ed6f81_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/submit_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'button_widget',  array('type' => isset($type) ? $type : 'submit')) ?>*/
/* */
