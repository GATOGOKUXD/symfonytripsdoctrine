<?php

/* @Framework/Form/number_widget.html.php */
class __TwigTemplate_b85596a724646637c7c037193fabc4f9cf77b953740a88e23f302c88c9f70d88 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_805c0a756285263b1b149fbcd76d8ac48b7286cf1455e027b32daefe9acd141b = $this->env->getExtension("native_profiler");
        $__internal_805c0a756285263b1b149fbcd76d8ac48b7286cf1455e027b32daefe9acd141b->enter($__internal_805c0a756285263b1b149fbcd76d8ac48b7286cf1455e027b32daefe9acd141b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/number_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple',  array('type' => isset(\$type) ? \$type : 'text')) ?>
";
        
        $__internal_805c0a756285263b1b149fbcd76d8ac48b7286cf1455e027b32daefe9acd141b->leave($__internal_805c0a756285263b1b149fbcd76d8ac48b7286cf1455e027b32daefe9acd141b_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/number_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple',  array('type' => isset($type) ? $type : 'text')) ?>*/
/* */
