<?php

/* @Framework/Form/textarea_widget.html.php */
class __TwigTemplate_1887bacbec83ab27c3a9e243a2d6b641c3aaf47fe649fbbd5a1884290e136051 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3eda8832377da80fbf617e9310fa2bb6429073f74b42093933036f3afab202cc = $this->env->getExtension("native_profiler");
        $__internal_3eda8832377da80fbf617e9310fa2bb6429073f74b42093933036f3afab202cc->enter($__internal_3eda8832377da80fbf617e9310fa2bb6429073f74b42093933036f3afab202cc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/textarea_widget.html.php"));

        // line 1
        echo "<textarea <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>><?php echo \$view->escape(\$value) ?></textarea>
";
        
        $__internal_3eda8832377da80fbf617e9310fa2bb6429073f74b42093933036f3afab202cc->leave($__internal_3eda8832377da80fbf617e9310fa2bb6429073f74b42093933036f3afab202cc_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/textarea_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <textarea <?php echo $view['form']->block($form, 'widget_attributes') ?>><?php echo $view->escape($value) ?></textarea>*/
/* */
