<?php

/* @Framework/FormTable/button_row.html.php */
class __TwigTemplate_d6f61ec5f6106bc24cc875177ce5688c7474e649ad1a86ad1256a0a7d9882bad extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3154c80ce361dec3f5f0757ea25ef36739103c7dc06e0681c54a04675d3eeb4a = $this->env->getExtension("native_profiler");
        $__internal_3154c80ce361dec3f5f0757ea25ef36739103c7dc06e0681c54a04675d3eeb4a->enter($__internal_3154c80ce361dec3f5f0757ea25ef36739103c7dc06e0681c54a04675d3eeb4a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/button_row.html.php"));

        // line 1
        echo "<tr>
    <td></td>
    <td>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $__internal_3154c80ce361dec3f5f0757ea25ef36739103c7dc06e0681c54a04675d3eeb4a->leave($__internal_3154c80ce361dec3f5f0757ea25ef36739103c7dc06e0681c54a04675d3eeb4a_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/button_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <tr>*/
/*     <td></td>*/
/*     <td>*/
/*         <?php echo $view['form']->widget($form) ?>*/
/*     </td>*/
/* </tr>*/
/* */
