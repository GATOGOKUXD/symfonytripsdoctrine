<?php

/* @Framework/Form/form_enctype.html.php */
class __TwigTemplate_ef441bba3c9d9d367e0b1ea46025dba002f3618fc94f461610818667547e40a7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_32790ad3938ffc03414bffde115d3e7f2c0aad7e9ab591a1288ec66a877b6d4b = $this->env->getExtension("native_profiler");
        $__internal_32790ad3938ffc03414bffde115d3e7f2c0aad7e9ab591a1288ec66a877b6d4b->enter($__internal_32790ad3938ffc03414bffde115d3e7f2c0aad7e9ab591a1288ec66a877b6d4b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_enctype.html.php"));

        // line 1
        echo "<?php if (\$form->vars['multipart']): ?>enctype=\"multipart/form-data\"<?php endif ?>
";
        
        $__internal_32790ad3938ffc03414bffde115d3e7f2c0aad7e9ab591a1288ec66a877b6d4b->leave($__internal_32790ad3938ffc03414bffde115d3e7f2c0aad7e9ab591a1288ec66a877b6d4b_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_enctype.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if ($form->vars['multipart']): ?>enctype="multipart/form-data"<?php endif ?>*/
/* */
