<?php

/* @Framework/Form/form_widget_simple.html.php */
class __TwigTemplate_0c7def1c8e26f442beda798ce3914c4bba9d494169f6252760f8e97f01ff14ec extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_990e76f4336fe26d7808690b2562542f061105481442b9590c07b148198adb3d = $this->env->getExtension("native_profiler");
        $__internal_990e76f4336fe26d7808690b2562542f061105481442b9590c07b148198adb3d->enter($__internal_990e76f4336fe26d7808690b2562542f061105481442b9590c07b148198adb3d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget_simple.html.php"));

        // line 1
        echo "<input type=\"<?php echo isset(\$type) ? \$view->escape(\$type) : 'text' ?>\" <?php echo \$view['form']->block(\$form, 'widget_attributes') ?><?php if (!empty(\$value) || is_numeric(\$value)): ?> value=\"<?php echo \$view->escape(\$value) ?>\"<?php endif ?> />
";
        
        $__internal_990e76f4336fe26d7808690b2562542f061105481442b9590c07b148198adb3d->leave($__internal_990e76f4336fe26d7808690b2562542f061105481442b9590c07b148198adb3d_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget_simple.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <input type="<?php echo isset($type) ? $view->escape($type) : 'text' ?>" <?php echo $view['form']->block($form, 'widget_attributes') ?><?php if (!empty($value) || is_numeric($value)): ?> value="<?php echo $view->escape($value) ?>"<?php endif ?> />*/
/* */
