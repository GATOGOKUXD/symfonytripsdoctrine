<?php

/* :trip:new.html.twig */
class __TwigTemplate_eeb27ff8abe9182e9e0df327fd6842551b372fdba4c3953dc8a776f17eee2dbc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":trip:new.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_13dc84fff838e797ad5ffcb0dc61caf94be6d6c6a34d2b2043c894be15bc93d5 = $this->env->getExtension("native_profiler");
        $__internal_13dc84fff838e797ad5ffcb0dc61caf94be6d6c6a34d2b2043c894be15bc93d5->enter($__internal_13dc84fff838e797ad5ffcb0dc61caf94be6d6c6a34d2b2043c894be15bc93d5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":trip:new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_13dc84fff838e797ad5ffcb0dc61caf94be6d6c6a34d2b2043c894be15bc93d5->leave($__internal_13dc84fff838e797ad5ffcb0dc61caf94be6d6c6a34d2b2043c894be15bc93d5_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_c04316a143d3d0f91d7a54079c8ca217e26bbccf0b9f600d09011ce04d0b8e7b = $this->env->getExtension("native_profiler");
        $__internal_c04316a143d3d0f91d7a54079c8ca217e26bbccf0b9f600d09011ce04d0b8e7b->enter($__internal_c04316a143d3d0f91d7a54079c8ca217e26bbccf0b9f600d09011ce04d0b8e7b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Trip creation</h1>


  <form class=\"form-horizontal\" method=\"post\" action=\"";
        // line 7
        echo $this->env->getExtension('routing')->getPath("trip_new");
        echo "\">
    <div class=\"form-group\">
        <label for=\"title\" class=\"col-sm-2 control-label\">NOMBRE</label>
        <div class=\"col-sm-10\">
            <input type=\"text\" class=\"form-control\" name=\"name\" id=\"title\" placeholder=\"Titulo\">
        </div>
    </div>
    <div class=\"form-group\">
        <label for=\"cover\" class=\"col-sm-2 control-label\">DESCRIPCION</label>
        <div class=\"col-sm-10\">
            <input type=\"text\" class=\"form-control\" id=\"cover\" name=\"description\" placeholder=\"URL de la portada\">
        </div>
    </div>
    <div class=\"form-group\">
        <label for=\"author\" class=\"col-sm-2 control-label\">URL IMAGEN</label>
        <div class=\"col-sm-10\">
            <input type=\"url\" class=\"form-control\" id=\"author\" name=\"url\" placeholder=\"Autor\">
        </div>
    </div>
    <div class=\"form-group\">
        <label for=\"price\" class=\"col-sm-2 control-label\">PRECIO</label>
        <div class=\"col-sm-10\">
            <input type=\"number\" class=\"form-control\" id=\"price\" name=\"price\" placeholder=\"Precio\">
        </div>
    </div>
    <div class=\"form-group\">
        <div class=\"col-sm-offset-2 col-sm-10\">
            <button type=\"submit\" class=\"btn btn-success pull-right\">Crear</button>
        </div>
    </div>
</form>
  

    <ul>
        <li>
            <a href=\"";
        // line 42
        echo $this->env->getExtension('routing')->getPath("trip_index");
        echo "\">Back to the list</a>
        </li>
    </ul>
";
        
        $__internal_c04316a143d3d0f91d7a54079c8ca217e26bbccf0b9f600d09011ce04d0b8e7b->leave($__internal_c04316a143d3d0f91d7a54079c8ca217e26bbccf0b9f600d09011ce04d0b8e7b_prof);

    }

    public function getTemplateName()
    {
        return ":trip:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  83 => 42,  45 => 7,  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends 'base.html.twig' %}*/
/* */
/* {% block body %}*/
/*     <h1>Trip creation</h1>*/
/* */
/* */
/*   <form class="form-horizontal" method="post" action="{{ path('trip_new') }}">*/
/*     <div class="form-group">*/
/*         <label for="title" class="col-sm-2 control-label">NOMBRE</label>*/
/*         <div class="col-sm-10">*/
/*             <input type="text" class="form-control" name="name" id="title" placeholder="Titulo">*/
/*         </div>*/
/*     </div>*/
/*     <div class="form-group">*/
/*         <label for="cover" class="col-sm-2 control-label">DESCRIPCION</label>*/
/*         <div class="col-sm-10">*/
/*             <input type="text" class="form-control" id="cover" name="description" placeholder="URL de la portada">*/
/*         </div>*/
/*     </div>*/
/*     <div class="form-group">*/
/*         <label for="author" class="col-sm-2 control-label">URL IMAGEN</label>*/
/*         <div class="col-sm-10">*/
/*             <input type="url" class="form-control" id="author" name="url" placeholder="Autor">*/
/*         </div>*/
/*     </div>*/
/*     <div class="form-group">*/
/*         <label for="price" class="col-sm-2 control-label">PRECIO</label>*/
/*         <div class="col-sm-10">*/
/*             <input type="number" class="form-control" id="price" name="price" placeholder="Precio">*/
/*         </div>*/
/*     </div>*/
/*     <div class="form-group">*/
/*         <div class="col-sm-offset-2 col-sm-10">*/
/*             <button type="submit" class="btn btn-success pull-right">Crear</button>*/
/*         </div>*/
/*     </div>*/
/* </form>*/
/*   */
/* */
/*     <ul>*/
/*         <li>*/
/*             <a href="{{ path('trip_index') }}">Back to the list</a>*/
/*         </li>*/
/*     </ul>*/
/* {% endblock %}*/
/* */
