<?php

/* @Framework/Form/choice_widget.html.php */
class __TwigTemplate_472db3c4e57ae5f11f095a1ebef86f4491e2bca0a4c1838c189f509417978713 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f6914acabc5fe9dc249231cdea136e90e1a62e3ee04d81b18f8eb103da43a4c5 = $this->env->getExtension("native_profiler");
        $__internal_f6914acabc5fe9dc249231cdea136e90e1a62e3ee04d81b18f8eb103da43a4c5->enter($__internal_f6914acabc5fe9dc249231cdea136e90e1a62e3ee04d81b18f8eb103da43a4c5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget.html.php"));

        // line 1
        echo "<?php if (\$expanded): ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_expanded') ?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_collapsed') ?>
<?php endif ?>
";
        
        $__internal_f6914acabc5fe9dc249231cdea136e90e1a62e3ee04d81b18f8eb103da43a4c5->leave($__internal_f6914acabc5fe9dc249231cdea136e90e1a62e3ee04d81b18f8eb103da43a4c5_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if ($expanded): ?>*/
/* <?php echo $view['form']->block($form, 'choice_widget_expanded') ?>*/
/* <?php else: ?>*/
/* <?php echo $view['form']->block($form, 'choice_widget_collapsed') ?>*/
/* <?php endif ?>*/
/* */
