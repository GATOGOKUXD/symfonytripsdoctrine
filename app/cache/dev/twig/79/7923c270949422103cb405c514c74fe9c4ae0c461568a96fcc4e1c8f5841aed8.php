<?php

/* @Framework/Form/button_widget.html.php */
class __TwigTemplate_946d46aca9de9236af02768f851cb7958e27a2b3edc88d82de2350e2189c098c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_eeebc37af33f3cfd65257da5506fb416ab36745d4cee214e3f0f88b4ded1eb2c = $this->env->getExtension("native_profiler");
        $__internal_eeebc37af33f3cfd65257da5506fb416ab36745d4cee214e3f0f88b4ded1eb2c->enter($__internal_eeebc37af33f3cfd65257da5506fb416ab36745d4cee214e3f0f88b4ded1eb2c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_widget.html.php"));

        // line 1
        echo "<?php if (!\$label) { \$label = isset(\$label_format)
    ? strtr(\$label_format, array('%name%' => \$name, '%id%' => \$id))
    : \$view['form']->humanize(\$name); } ?>
<button type=\"<?php echo isset(\$type) ? \$view->escape(\$type) : 'button' ?>\" <?php echo \$view['form']->block(\$form, 'button_attributes') ?>><?php echo \$view->escape(false !== \$translation_domain ? \$view['translator']->trans(\$label, array(), \$translation_domain) : \$label) ?></button>
";
        
        $__internal_eeebc37af33f3cfd65257da5506fb416ab36745d4cee214e3f0f88b4ded1eb2c->leave($__internal_eeebc37af33f3cfd65257da5506fb416ab36745d4cee214e3f0f88b4ded1eb2c_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/button_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if (!$label) { $label = isset($label_format)*/
/*     ? strtr($label_format, array('%name%' => $name, '%id%' => $id))*/
/*     : $view['form']->humanize($name); } ?>*/
/* <button type="<?php echo isset($type) ? $view->escape($type) : 'button' ?>" <?php echo $view['form']->block($form, 'button_attributes') ?>><?php echo $view->escape(false !== $translation_domain ? $view['translator']->trans($label, array(), $translation_domain) : $label) ?></button>*/
/* */
