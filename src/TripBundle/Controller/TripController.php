<?php

namespace TripBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use TripBundle\Entity\Trip;
use TripBundle\Form\TripType;

/**
 * Trip controller.
 * @Route("/trip")
 */
class TripController extends Controller
{
    /**
     * Lists all Trip entities.
     * @Route("/", name="trip_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $trips = $em->getRepository('TripBundle:Trip')->findAll();
        return $this->render('trip/index.html.twig', array(
            'trips' => $trips,
        ));
    }
     /**
     * Muestra el formulario de creación de libros.
      * @Route("/create", name="create_action")
     */
    public function createAction() {
        return $this->render('trip/new.html.twig');
    }
    /**
     * Creates a new Trip entity.
     * @Route("/new", name="trip_new")
     * @Method({"GET", "POST"})
     */
      public function newAction(Request $request)
    {
          $postData = $request->request;
        $trip = new Trip(null ,$postData->get("name"), $postData->get("description"),  $postData->get("url"), $postData->get("price"));
        $em = $this->getDoctrine()->getManager();
        $em->persist($trip);
        $em->flush();
        $form = $this->createForm(new TripType(), $trip);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($trip);
            $em->flush();
            return $this->redirectToRoute('trip_show', array('id' => $trip->getId()));
        }
        return $this->render('trip/new.html.twig', array(
            'trip' => $trip,
            'form' => $form->createView(),
        ));
    }
  
    /**
     * Finds and displays a Trip entity.
     * @Route("/{id}/show", name="trip_show")
     * @Method("GET")
     */
    public function showAction(Trip $trip)
    {
        $deleteForm = $this->createDeleteForm($trip);
        return $this->render('trip/show.html.twig', array(
            'trip' => $trip,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Trip entity.
     * @Route("/{id}/update", name="trip_update")
     * @Method({"GET", "POST"})
     */
    public function updateAction(Request $request ,$id)
    {
  $postData = $request->request;
        $trip = $this->getDoctrine()->getRepository('TripBundle:Trip')
                ->find($id);
        $trip->setName($postData->get("name"));
        $trip->setUrlPicture($postData->get("url"));
        $trip->setDescription($postData->get("description"));
        $trip->setPrice($postData->get("price"));
        $em = $this->getDoctrine()->getManager();
        $em->merge($trip);
        $em->flush();
        return $this->forward('TripBundle:Trip:index');
    }
    
     /**
     * Displays a form to edit an existing Trip entity.
     * @Route("/search/{busqueda}", name="trip_search")
     * @Method("GET")
     */
    public function searchAction(Request $request ,$busqueda)
    {
  $termino = $request->get("trip");
   $em = $this->getDoctrine()->getManager();
$query = $em->createQuery(
                'SELECT t
                FROM TripBundle:Trip t
                WHERE t.name like :search or t.description like :search or t.price = :search
                ORDER BY t.name ASC'
                )->setParameter('search',$termino);//'%'.$request->get('busqueda').'%' );
$trips = $query->getResult();
      return $this->render('trip/index.html.twig', array('trips' => $trips));
    }
 
      /**
     * Displays a form to edit an existing Trip entity.
     * @Route("/{id}/edit", name="trip_edit")
     */
    public function editAction($id)
    {
        $trip = $this->getDoctrine()->getRepository('TripBundle:Trip')->find($id);  
        if ($trip) {  return $this->render('trip/edit.html.twig', array('trip' => $trip));
        } else {
         // return $this->forward('TripBundle:Trip:index'); }
            }
        
    }
    /**
     * Deletes a Trip entity.
     * @Route("/{id}", name="trip_delete")
      * @Method({"GET", "POST"})
     */
     private function deleteAction(Request $request, Trip $trip)
    {
        $form = $this->createDeleteForm($trip);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($trip);
            $em->flush();
        }
        return $this->redirectToRoute('trip_index');
    }

    /**
     * Creates a form to delete a Trip entity.
     * @param Trip $trip The Trip entity
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Trip $trip)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('trip_delete', array('id' => $trip->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
